export const sliderItems = [
  {
    id: 1,
    prodImage: "img/slider.webp",
    title: "EXCITING PLACE",
    desc: "FOR THE WHOLE FAMILY TO SHOP",
    bg: "000",
  },
  {
    id: 2,
    prodImage:
      "https://images.unsplash.com/photo-1519638831568-d9897f54ed69?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=870&q=80",
    title: "CAPTURE MOMENTS",
    desc: "THE GIFT THEY ARE WISHING FOR IS RIGHT HERE",
    bg: "000",
  },
];

export const categories = [
  {
    id: 1,
    prodImage:
      "https://images.unsplash.com/photo-1581591524425-c7e0978865fc?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=870&q=80",
    title: "CAMERA!",
    cat: "Camera",
  },
  {
    id: 2,
    prodImage:
      "https://images.unsplash.com/photo-1611078489935-0cb964de46d6?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=774&q=80",
    title: "LAPTOPS",
    cat: "Laptop",
  },
  {
    id: 3,
    prodImage:
      "https://images.unsplash.com/photo-1567581935884-3349723552ca?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=774&q=80",
    title: "MOBILE",
    cat: "Mobile",
  },
];
