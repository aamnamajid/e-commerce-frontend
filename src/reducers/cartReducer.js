import { ADD_ITEM_TO_CART, REMOVE_ITEM_FROM_CART, SHIPPING_ADDRESS } from "../constants/cartConstants";

export const cartReducer = (state = {orderItems: [], shippingAdress:{} }, action) =>{
    switch(action.type) {
        case ADD_ITEM_TO_CART:
            const item = action.payload
            const item_exists = state.orderItems.find(x=>x.productID === item.productID)
            
            if(item_exists) {
                return {
                    ...state,
                    orderItems: state.orderItems.map(x => x.productID === item_exists.productID ? item : x)
                }
            }
            else {
                return {
                    ...state,
                    orderItems: [...state.orderItems, item]
                }
            }
        
        case REMOVE_ITEM_FROM_CART:
            return {
                ...state,
                orderItems: state.orderItems.filter((x) => x.products !== action.payload)
            }

        case SHIPPING_ADDRESS:
            return {
                ...state,
                shippingAdress: action.payload
            }

        default:
            return state
    }
}


