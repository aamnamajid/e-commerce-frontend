import {
  SUCCESSFUL_GET_PRODUCT,
  FAILED_GET_PRODUCT,
  PRODUCTDETAIL_GET_SUCCESS,
  PRODUCTDETAIL_GET_FAILED,
  TOP_PRODUCTS_SUCCESSFUL,
  TOP_PRODUCTS_FAILED,
  ADMIN_DEL_PROD_SUCCESSFUL,
  ADMIN_DEL_PROD_FAILED,
  ADMIN_CREATE_PROD_SUCCESSFUL,
  ADMIN_CREATE_PROD_FAILED,
  ADMIN_PRODUCT_GET_SUCCESS,
  ADMIN_PRODUCT_GET_FAILED,
  ADMIN_UPDATE_PROD_SUCCESSFUL,
  ADMIN_UPDATE_PROD_FAILED,
  PRODUCT_ADD_REVIEW_SUCCESSFUL,
  PRODUCT_ADD_REVIEW_FAILED,
} from "../constants/productConstants";

export const productReducer = (state = { products: [] }, action) => {
  switch (action.type) {
    case SUCCESSFUL_GET_PRODUCT:
      return {
        products: action.payload.products,
        pages: action.payload.pages,
        page: action.payload.page,
      };
    case FAILED_GET_PRODUCT:
      return {
        error: action.payload,
      };
    default:
      return state;
  }
};

export const productDetailReducer = (state = { products: {} }, action) => {
  switch (action.type) {
    case PRODUCTDETAIL_GET_SUCCESS:
      return {
        products: action.payload,
      };
    case PRODUCTDETAIL_GET_FAILED:
      return {
        error: action.payload,
      };
    default:
      return state;
  }
};

export const TopProductsReducer = (state = { products: [] }, action) => {
  switch (action.type) {
    case TOP_PRODUCTS_SUCCESSFUL:
      return {
        products: action.payload,
      };
    case TOP_PRODUCTS_FAILED:
      return {
        error: action.payload,
      };
    default:
      return state;
  }
};

export const delProduct = (state = {}, action) => {
  switch (action.type) {
    case ADMIN_DEL_PROD_SUCCESSFUL:
      return {
        sucess: true,
      };
    case ADMIN_DEL_PROD_FAILED:
      return {
        error: action.payload,
      };
    default:
      return state;
  }
};

export const adminCreateProduct = (state = {}, action) => {
  switch (action.type) {
    case ADMIN_CREATE_PROD_SUCCESSFUL:
      return { success: true };
    case ADMIN_CREATE_PROD_FAILED:
      return { error: action.payload };
    default:
      return state;
  }
};

export const adminGetProductReducer = (state = { products: [] }, action) => {
  switch (action.type) {
    case ADMIN_PRODUCT_GET_SUCCESS:
      return {
        products: action.payload.products,
      };
    case ADMIN_PRODUCT_GET_FAILED:
      return {
        error: action.payload,
      };
    default:
      return state;
  }
};

export const adminUpdateProduct = (state = {}, action) => {
  switch (action.type) {
    case ADMIN_UPDATE_PROD_SUCCESSFUL:
      return { success: true, updatedProduct: action.payload };
    case ADMIN_UPDATE_PROD_FAILED:
      return { error: action.payload };
    default:
      return state;
  }
};

export const addReviewReducer = (state = {}, action) => {
  switch (action.type) {
    case PRODUCT_ADD_REVIEW_SUCCESSFUL:
      return {};
    case PRODUCT_ADD_REVIEW_FAILED:
      return {
        error: action.payload,
      };
    default:
      return state;
  }
};
