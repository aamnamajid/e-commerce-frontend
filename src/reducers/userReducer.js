import {
  USER_LOGIN_SUCCESS,
  USER_LOGIN_FAIL,
  USER_REGISTER_SUCCESS,
  USER_REGISTER_FAIL,
  ADMIN_USER_LIST_SUCCESS,
  ADMIN_USER_LIST_FAIL,
  USER_DETAILS_SUCCESS,
  USER_DETAILS_FAIL,
  USER_PROFILE_UPDATE_SUCCESS,
  USER_PROFILE_UPDATE_FAIL,
  ADMIN_DELETE_USER_SUCCESS,
  ADMIN_DELETE_USER_FAIL,
  ADMIN_UPDATE_USER_SUCCESS,
  ADMIN_UPDATE_USER_FAIL,
  ADMIN_CREATE_USER_SUCCESS,
  ADMIN_CREATE_USER_FAIL,
} from "../constants/userConstants";

export const userLoginReducer = (state = {}, action) => {
  switch (action.type) {
    case USER_LOGIN_SUCCESS:
      return { userInfo: action.payload };
    case USER_LOGIN_FAIL:
      return { error: action.payload };

    default:
      return state;
  }
};

export const userRegisterReducer = (state = {}, action) => {
  switch (action.type) {
    case USER_REGISTER_SUCCESS:
      return { userInfo: action.payload };
    case USER_REGISTER_FAIL:
      return { error: action.payload };

    default:
      return state;
  }
};

export const userDetailReducer = (state = { user: {} }, action) => {
  switch (action.type) {
    case USER_DETAILS_SUCCESS:
      return { user: action.payload };
    case USER_DETAILS_FAIL:
      return { error: action.payload };

    default:
      return state;
  }
};

export const userUpdateProfileReducer = (state = {}, action) => {
  switch (action.type) {
    case USER_PROFILE_UPDATE_SUCCESS:
      return { userUpdated: action.payload };
    case USER_PROFILE_UPDATE_FAIL:
      return { error: action.payload };

    default:
      return state;
  }
};

export const adminGetUserList = (state = { users: [] }, action) => {
  switch (action.type) {
    case ADMIN_USER_LIST_SUCCESS:
      return { users: action.payload };
    case ADMIN_USER_LIST_FAIL:
      return { error: action.payload };
    default:
      return state;
  }
};

export const adminDelUser = (state = {}, action) => {
  switch (action.type) {
    case ADMIN_DELETE_USER_SUCCESS:
      return { success: true };
    case ADMIN_DELETE_USER_FAIL:
      return { error: action.payload };
    default:
      return state;
  }
};

export const adminUpdateUser = (state = {}, action) => {
  switch (action.type) {
    case ADMIN_UPDATE_USER_SUCCESS:
      return { success: true, updatedUser: action.payload };
    case ADMIN_UPDATE_USER_FAIL:
      return { error: action.payload };
    default:
      return state;
  }
};

export const adminCreateUser = (state = {}, action) => {
  switch (action.type) {
    case ADMIN_CREATE_USER_SUCCESS:
      return { success: true };
    case ADMIN_CREATE_USER_FAIL:
      return { error: action.payload };
    default:
      return state;
  }
};
