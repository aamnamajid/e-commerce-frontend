import {
  ADMIN_GET_ORDERS_FAILED,
  ADMIN_GET_ORDERS_SUCCESSFULL,
  ORDER_LIST_FAILED,
  ORDER_LIST_SUCCESSFULL,
  ORDER_DETAILS_SUCCESSFUL,
  ORDER_DETAILS_FAILED,
  ORDER_FAILED,
  ORDER_SUCCESSFUL,
  ADMIN_DELIVERED_ORDERS_SUCCESSFULL,
  ADMIN_DELIVERED_ORDERS_FAILED,
  ADMIN_DELIVERED_ORDERS_RESET,
  ORDER_PAY_SUCCESSFUL,
  ORDER_PAY_FAILED,
  ORDER_PAY_RESET,
  ORDER_CREATE_RESET,
  ORDER_DELETE_SUCCESSFUL,
  ORDER_DELETE_FAILED,
} from "../constants/orderConstants";

export const orderReducer = (state = {}, action) => {
  switch (action.type) {
    case ORDER_SUCCESSFUL:
      return {
        success: true,
        order: action.payload,
      };
    case ORDER_FAILED:
      return {
        error: action.payload,
      };
    case ORDER_CREATE_RESET:
      return {};
    default:
      return state;
  }
};

export const orderDetailsReducer = (state = { order: {} }, action) => {
  switch (action.type) {
    case ORDER_DETAILS_SUCCESSFUL:
      return {
        order: action.payload,
      };
    case ORDER_DETAILS_FAILED:
      return {
        error: action.payload,
      };
    default:
      return state;
  }
};

export const orderListReducer = (state = { orders: [] }, action) => {
  switch (action.type) {
    case ORDER_LIST_SUCCESSFULL:
      return {
        orders: action.payload,
      };
    case ORDER_LIST_FAILED:
      return {
        error: action.payload,
      };
    default:
      return state;
  }
};

export const adminOrderListReducer = (state = { orders: [] }, action) => {
  switch (action.type) {
    case ADMIN_GET_ORDERS_SUCCESSFULL:
      return {
        orders: action.payload,
      };
    case ADMIN_GET_ORDERS_FAILED:
      return {
        error: action.payload,
      };
    default:
      return state;
  }
};

export const adminOrderDeliverReducer = (
  state = { success: false },
  action
) => {
  switch (action.type) {
    case ADMIN_DELIVERED_ORDERS_SUCCESSFULL:
      return {
        success: true,
      };
    case ADMIN_DELIVERED_ORDERS_FAILED:
      return {
        error: action.payload,
      };
    case ADMIN_DELIVERED_ORDERS_RESET:
      return {};
    default:
      return state;
  }
};

export const orderPayReducer = (state = { success: false }, action) => {
  switch (action.type) {
    case ORDER_PAY_SUCCESSFUL:
      return {
        success: true,
      };
    case ORDER_PAY_FAILED:
      return {
        error: action.payload,
      };
    case ORDER_PAY_RESET:
      return {};
    default:
      return state;
  }
};

export const adminDelOrder = (state = {}, action) => {
  switch (action.type) {
    case ORDER_DELETE_SUCCESSFUL:
      return { success: true };
    case ORDER_DELETE_FAILED:
      return { error: action.payload };
    default:
      return state;
  }
};
