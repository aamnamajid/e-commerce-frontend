import "./App.css";
import { Routes, Route, BrowserRouter as Router } from "react-router-dom";
import HomeScreen from "./screens/HomeScreen";
import LoginScreen from "./screens/LoginScreen";
import RegisterUser from "./screens/RegisterScreen.js";
import Header from "./components/Header";
import AllProducts from "./components/AllProducts.js";
import ProductDetail from "./screens/ProductDetail";
import Cart from "./screens/Cart";
import Shipping from "./screens/Shipping";
import PlaceOrder from "./screens/PlaceOrder";
import OrderScreen from "./screens/OrderScreen";
import Footer from "./components/Footer.js";
import Profile from "./screens/Profile";
import MyOrder from "./screens/MyOrder";
import Admin from "./screens/admin/admin";
import CategoryScreen from "./screens/CategoryScreen";
function App() {
  return (
    <div className="App">
      <div id="content-wrap">
        <Router>
          <Header />

          <Routes>
            <Route exact path="admin/*" element={<Admin />} />
            <>
              <Route exact path="/" element={<HomeScreen />} />
              <Route path="/register" element={<RegisterUser />} />
              <Route path="/login" element={<LoginScreen />} />
              <Route path="/products" exact element={<AllProducts />} />
              <Route path="/product/:id" element={<ProductDetail />} />
              <Route
                path="/products/page/:pageNumber"
                element={<AllProducts />}
              />
              <Route path="/products/:keyword" element={<CategoryScreen />} />

              <Route path="/search/:keyword" element={<AllProducts />} />
              <Route
                path="/search/:keyword/page/:pageNumber"
                element={<AllProducts />}
              />
              <Route path="/order/shipping" element={<Shipping />} />
              <Route path="/order/place-order" element={<PlaceOrder />} />
              <Route path="/order/:orderId" element={<OrderScreen />} />
              <Route exact path="/cart" element={<Cart />} />
              <Route path="/cart/:id" element={<Cart />} />
              <Route path="/profile" element={<Profile />} />
              <Route path="/profile/myorders" element={<MyOrder />} />
            </>
          </Routes>
        </Router>
      </div>
      <Footer />
    </div>
  );
}

export default App;
