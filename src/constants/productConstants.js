export const SUCCESSFUL_GET_PRODUCT = "SUCCESSFUL_GET_PRODUCT";
export const FAILED_GET_PRODUCT = "FAILED_GET_PRODUCT";

export const PRODUCTDETAIL_GET_SUCCESS = "PRODUCTDETAIL_GET_SUCCESS";
export const PRODUCTDETAIL_GET_FAILED = "PRODUCTDETAIL_GET_FAILED";

export const TOP_PRODUCTS_SUCCESSFUL = "TOP_PRODUCTS_SUCCESSFUL";
export const TOP_PRODUCTS_FAILED = "TOP_PRODUCTS_FAILED";

export const ADMIN_DEL_PROD_SUCCESSFUL = "ADMIN_DEL_PROD_SUCCESSFUL";
export const ADMIN_DEL_PROD_FAILED = "ADMIN_DEL_PROD_FAILED";

export const ADMIN_CREATE_PROD_SUCCESSFUL = "ADMIN_CREATE_PROD_SUCCESSFUL";
export const ADMIN_CREATE_PROD_FAILED = "ADMIN_CREATE_PROD_FAILED";

export const ADMIN_UPDATE_PROD_SUCCESSFUL = "ADMIN_UPDATE_PROD_SUCCESSFUL";
export const ADMIN_UPDATE_PROD_FAILED = "ADMIN_UPDATE_PROD_FAILED";

export const ADMIN_PRODUCT_GET_SUCCESS = "ADMIN_PRODUCT_GET_SUCCESS";
export const ADMIN_PRODUCT_GET_FAILED = "ADMIN_PRODUCT_GET_FAILED";

export const PRODUCT_ADD_REVIEW_SUCCESSFUL = "PRODUCT_ADD_REVIEW_SUCCESSFUL";
export const PRODUCT_ADD_REVIEW_FAILED = "PRODUCT_ADD_REVIEW_FAILED";
