import {
  SUCCESSFUL_GET_PRODUCT,
  FAILED_GET_PRODUCT,
  PRODUCTDETAIL_GET_SUCCESS,
  PRODUCTDETAIL_GET_FAILED,
  TOP_PRODUCTS_SUCCESSFUL,
  TOP_PRODUCTS_FAILED,
  ADMIN_DEL_PROD_SUCCESSFUL,
  ADMIN_DEL_PROD_FAILED,
  ADMIN_CREATE_PROD_SUCCESSFUL,
  ADMIN_CREATE_PROD_FAILED,
  ADMIN_PRODUCT_GET_SUCCESS,
  ADMIN_PRODUCT_GET_FAILED,
  ADMIN_UPDATE_PROD_SUCCESSFUL,
  ADMIN_UPDATE_PROD_FAILED,
} from "../constants/productConstants";
import axios from "axios";
import API_URL from "../config/urls.js";
export const getAllProducts =
  (keyword = "", pageNumber = "") =>
  async (dispatch) => {
    try {
      const { data } = await axios.get(
        API_URL + `products?keyword=${keyword}&pageNumber=${pageNumber}`
      );
      dispatch({
        type: SUCCESSFUL_GET_PRODUCT,
        payload: data,
      });
    } catch (error) {
      dispatch({
        type: FAILED_GET_PRODUCT,
        payload: error,
      });
    }
  };

export const getAllProductsAdmin = () => async (dispatch, getState) => {
  try {
    const {
      userLogin: { userInfo },
    } = getState();

    const config = {
      headers: {
        Authorization: `Bearer ${userInfo.token}`,
      },
    };
    const { data } = await axios.get(API_URL + "admin/products", config);
    dispatch({
      type: ADMIN_PRODUCT_GET_SUCCESS,
      payload: data,
    });
  } catch (error) {
    dispatch({
      type: ADMIN_PRODUCT_GET_FAILED,
      payload: error,
    });
  }
};

export const getProductDetail = (id) => async (dispatch) => {
  try {
    const { data } = await axios.get(API_URL + `products/${id}`);
    dispatch({
      type: PRODUCTDETAIL_GET_SUCCESS,
      payload: data,
    });
  } catch (error) {
    dispatch({
      type: PRODUCTDETAIL_GET_FAILED,
      payload: error,
    });
  }
};

export const getTopProducts = () => async (dispatch) => {
  try {
    const { data } = await axios.get(API_URL + `products/top`);

    dispatch({
      type: TOP_PRODUCTS_SUCCESSFUL,
      payload: data,
    });
  } catch (error) {
    dispatch({
      type: TOP_PRODUCTS_FAILED,
      payload: error,
    });
  }
};

export const deleteProduct = (id) => async (dispatch, getState) => {
  const {
    userLogin: { userInfo },
  } = getState();

  const config = {
    headers: {
      Authorization: `Bearer ${userInfo.token}`,
    },
  };
  try {
    await axios.delete(API_URL + `products/${id}`, config);
    dispatch({
      type: ADMIN_DEL_PROD_SUCCESSFUL,
    });
  } catch (error) {
    dispatch({
      type: ADMIN_DEL_PROD_FAILED,
      payload: error,
    });
  }
};

export const updateProduct = (product) => async (dispatch, getState) => {
  try {
    const {
      userLogin: { userInfo },
    } = getState();
    const config = {
      headers: {
        Authorization: `Bearer ${userInfo.token}`,
        "Content-Type": "application/json",
      },
    };

    const { data } = await axios.put(
      API_URL + `products/${product._id}`,
      product,
      config
    );
    dispatch({
      type: ADMIN_UPDATE_PROD_SUCCESSFUL,
      payload: data,
    });
  } catch (error) {
    dispatch({
      type: ADMIN_UPDATE_PROD_FAILED,
      payload:
        error.response && error.response.data.message
          ? error.response.data.message
          : error.message,
    });
  }
};

export const createProductByAdmin = (data) => async (dispatch, getState) => {
  try {
    const {
      userLogin: { userInfo },
    } = getState();

    const config = {
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${userInfo.token}`,
      },
    };

    const { dta } = await axios.post(API_URL + "products", data, config);
    dispatch({
      type: ADMIN_CREATE_PROD_SUCCESSFUL,
      payload: dta,
    });
  } catch (error) {
    dispatch({
      type: ADMIN_CREATE_PROD_FAILED,
      payload: error.message,
    });
  }
};

export const addProductReview =
  (productId, review) => async (dispatch, getState) => {
    try {
      const {
        userLogin: { userInfo },
      } = getState();

      const config = {
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${userInfo.token}`,
        },
      };
      await axios.post(
        API_URL + `products/${productId}/review`,
        review,
        config
      );

      dispatch({
        type: TOP_PRODUCTS_SUCCESSFUL,
      });
    } catch (error) {
      dispatch({
        type: TOP_PRODUCTS_FAILED,
        payload: error,
      });
    }
  };
