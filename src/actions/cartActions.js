import axios from 'axios'
import { ADD_ITEM_TO_CART, REMOVE_ITEM_FROM_CART, SHIPPING_ADDRESS } from '../constants/cartConstants'
import API_URL from '../config/urls.js'

export const addToCart=(id, qty)=> async(dispatch, getState)=>{

    const {data} = await axios.get(API_URL + `products/${id}`)
    dispatch({
        type: ADD_ITEM_TO_CART,
        payload: {
            productID: data._id,
            name: data.name,
            image: data.prodImage,
            price: data.price,
            countInStock: data.countInStock,
            qty
        }
    })
    localStorage.setItem('orderItems', JSON.stringify(getState().cart.orderItems))
}

export const removeFromCart=(id) => (dispatch, getState)=>{
    dispatch({
        type: REMOVE_ITEM_FROM_CART,
        payload: id
    })
    localStorage.removeItem('orderItems', JSON.stringify(getState().cart.orderItems))
}

export const shippingAddress = (data) => (dispatch) => {
    dispatch({
        type: SHIPPING_ADDRESS,
        payload: data
    })

    localStorage.setItem('shippingAddress', JSON.stringify(data))
}