import API_URL from "../config/urls";
import {
  ORDER_FAILED,
  ORDER_SUCCESSFUL,
  ORDER_DETAILS_FAILED,
  ORDER_DETAILS_SUCCESSFUL,
  ADMIN_GET_ORDERS_FAILED,
  ADMIN_GET_ORDERS_SUCCESSFULL,
  ORDER_LIST_FAILED,
  ORDER_LIST_SUCCESSFULL,
  ADMIN_DELIVERED_ORDERS_SUCCESSFULL,
  ADMIN_DELIVERED_ORDERS_FAILED,
  ORDER_PAY_SUCCESSFUL,
  ORDER_PAY_FAILED,
  ORDER_DELETE_SUCCESSFUL,
  ORDER_DELETE_FAILED,
} from "../constants/orderConstants";
import axios from "axios";

export const createOrder = (order) => async (dispatch, getState) => {
  try {
    const {
      userLogin: { userInfo },
    } = getState();

    const config = {
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${userInfo.token}`,
      },
    };

    const { data } = await axios.post(API_URL + "order/", order, config);
    dispatch({
      type: ORDER_SUCCESSFUL,
      payload: data,
    });
    localStorage.removeItem("orderItems");
    localStorage.removeItem("shippingAddress");
  } catch (error) {
    dispatch({
      type: ORDER_FAILED,
      payload: error,
    });
  }
};

export const getOrderDetails = (id) => async (dispatch, getState) => {
  try {
    const {
      userLogin: { userInfo },
    } = getState();

    const config = {
      headers: {
        Authorization: `Bearer ${userInfo.token}`,
      },
    };

    const { data } = await axios.get(API_URL + `order/${id}`, config);
    dispatch({
      type: ORDER_DETAILS_SUCCESSFUL,
      payload: data,
    });
  } catch (error) {
    dispatch({
      type: ORDER_DETAILS_FAILED,
      payload: error.response.data.message,
    });
  }
};

export const myOrdersList = () => async (dispatch, getState) => {
  try {
    const {
      userLogin: { userInfo },
    } = getState();

    const config = {
      headers: {
        Authorization: `Bearer ${userInfo.token}`,
      },
    };

    const { data } = await axios.get(API_URL + `order/myorders`, config);
    dispatch({
      type: ORDER_LIST_SUCCESSFULL,
      payload: data,
    });
  } catch (error) {
    dispatch({
      type: ORDER_LIST_FAILED,
      payload: error,
    });
  }
};

export const getAllOrdersAdmin = () => async (dispatch, getState) => {
  try {
    const {
      userLogin: { userInfo },
    } = getState();

    const config = {
      headers: {
        Authorization: `Bearer ${userInfo.token}`,
      },
    };
    const { data } = await axios.get(API_URL + "order", config);
    dispatch({
      type: ADMIN_GET_ORDERS_SUCCESSFULL,
      payload: data,
    });
  } catch (error) {
    dispatch({
      type: ADMIN_GET_ORDERS_FAILED,
      payload: error,
    });
  }
};

export const markOrderDeliver = (order) => async (dispatch, getState) => {
  try {
    const {
      userLogin: { userInfo },
    } = getState();

    const config = {
      headers: {
        Authorization: `Bearer ${userInfo.token}`,
      },
    };
    const { data } = await axios.put(
      API_URL + `order/${order._id}/delievered`,
      config
    );
    dispatch({
      type: ADMIN_DELIVERED_ORDERS_SUCCESSFULL,
      payload: data,
    });
  } catch (error) {
    dispatch({
      type: ADMIN_DELIVERED_ORDERS_FAILED,
      payload: error.response.data.message,
    });
  }
};

export const markOrderPaid =
  (orderId, paymentResult) => async (dispatch, getState) => {
    try {
      const {
        userLogin: { userInfo },
      } = getState();

      const config = {
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${userInfo.token}`,
        },
      };
      const { data } = await axios.put(
        API_URL + `order/${orderId}/paid`,
        paymentResult,
        config
      );
      dispatch({
        type: ORDER_PAY_SUCCESSFUL,
        payload: data,
      });
    } catch (error) {
      dispatch({
        type: ORDER_PAY_FAILED,
        payload: error.response.data.message,
      });
    }
  };

export const deleteOrder = (id) => async (dispatch, getState) => {
  const {
    userLogin: { userInfo },
  } = getState();

  const config = {
    headers: {
      Authorization: `Bearer ${userInfo.token}`,
    },
  };
  try {
    await axios.delete(API_URL + `order/${id}`, config);
    dispatch({
      type: ORDER_DELETE_SUCCESSFUL,
    });
  } catch (error) {
    dispatch({
      type: ORDER_DELETE_FAILED,
      payload: error,
    });
  }
};
