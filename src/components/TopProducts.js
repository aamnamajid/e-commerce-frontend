import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { getTopProducts } from "../actions/productAction";
import styled from "styled-components";
import { Center, Logo } from "./styledComponents/styled";
import { Button } from "./styledComponents/global.styled";
import { mobile } from "../components/responsive";
const Image = styled.img`
  height: 100%;
`;
const Container = styled.div`
  display: flex;
  padding: 0 50px;
  flex-wrap: wrap;
  flex-direction: row;
  justify-content: space-evenly;
  align-items: center;
  position: relative;
  margin-bottom: 50px;
`;

export const CardWrapper = styled.div`
  overflow: hidden;
  padding: 0 0 120px 0;
  margin: 48px auto;
  width: 16rem;
  height: 45vh;
  border-radius: 5px;
  border: 1px solid #ececec;

  ${mobile({ width: "14rem" })}
`;

export const CardTitle = styled.div`
  font-size: 18px;
  color: black;
`;
export const CardText = styled.div`
  font-size: 14px;
`;

const TopProducts = () => {
  const dispatch = useDispatch();
  const topProds = useSelector((state) => state.topProducts);
  const { products } = topProds;

  useEffect(() => {
    dispatch(getTopProducts());
  }, [dispatch]);
  if (!products) return <p></p>;
  return (
    <>
      <Center>
        <Logo>TOP PRODUCTS</Logo>
        <Container>
          {products.map((product) => (
            <CardWrapper key={product._id}>
              <Image src={product.prodImage} />
              <CardTitle> {product.name}</CardTitle>
              <CardText>Rs {product.price}</CardText>
              <Button as="a" href={`/product/${product._id}`}>
                {" "}
                View Product
              </Button>
            </CardWrapper>
          ))}
        </Container>
      </Center>
    </>
  );
};

export default TopProducts;
