import React from "react";
import { Pagination } from "react-bootstrap";
import { useNavigate } from "react-router-dom";
import { Nav } from "react-bootstrap";

const Paginate = ({ pages, page, isAdmin = false, keyword = " " }) => {
  const navigate = useNavigate();
  return (
    pages > 1 && (
      <Pagination>
        {[...Array(pages).keys()].map((x) => (
          <Nav.Link
            key={x + 1}
            onClick={() =>
              navigate(
                keyword
                  ? `/search/${keyword}/page/${x + 1}`
                  : `/products/page/${x + 1}`
              )
            }
          >
            <Pagination.Item>{x + 1}</Pagination.Item>
          </Nav.Link>
        ))}
      </Pagination>
    )
  );
};

export default Paginate;
