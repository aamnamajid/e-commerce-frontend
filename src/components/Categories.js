import styled from "styled-components";
import { categories } from "../data";
import { mobile } from "./responsive";
import CategoryItem from "./CategoryItem";
import { Logo } from "./styledComponents/styled";

const Container = styled.div`
  display: flex;
  padding: 20px 50px;
  justify-content: space-between;
  ${mobile({ padding: "0px", flexDirection: "column" })}
`;

const Categories = () => {
  return (
    <div div className="mt-3">
      <Logo>CATEGORIES</Logo>

      <Container>
        {categories.map((item) => (
          <CategoryItem item={item} key={item.id} />
        ))}
      </Container>
    </div>
  );
};

export default Categories;
