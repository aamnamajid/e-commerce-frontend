import styled from "styled-components";
import { Link } from "react-router-dom";
export const Container = styled.div`
  width: 100%;
  padding-right: 50px;
  padding-left: 50px;
  margin-right: auto;
  margin-left: auto;
  margin-top: 80px;
`;
export const Row = styled.div`
  display: flex;
  @media (max-width: 786px) {
    flex-direction: column;
  }
`;

export const FormSVG = styled.img`
  width: 100%;
  margin-top: 30px;
`;

export const Col = styled.div`
  flex: ${(props) => props.size};
`;

export const FormGroup = styled.div`
  padding: 0 80px;
`;
export const Title = styled.h4`
  font-weight: bold;
  text-align: center;
  margin-top: 30px;
  margin-bottom: 20px;
`;
export const Input = styled.input`
  display: block;
  width: 100%;
  padding: 0.375rem 0.75rem;
  font-size: 1rem;
  line-height: 1.5;
  color: #495057;
  background-color: #fff;
  background-clip: padding-box;
  border: 1px solid #ced4da;
  border-radius: 0.25rem;
  transition: border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;
`;
export const FormLabel = styled.label`
  width: 100%;
  margin-top: 1rem;
  text-align: left !important;
`;

export const Button = styled.button`
  display: inline-block;
  background: #d10024;
  color: white;
  font-size: 1em;
  margin: 1em;
  padding: 0.25em 1em;
  border: 2px solid #d10024;
  border-radius: 3px;
  &:hover {
    cursor: pointer;
    text-decoration: none;
    color: white;
    opacity: 0.9;
    transition: 0.3s;  
`;

export const LinkPage = styled(Link)`
  color: #d10024;
  &:hover {
    text-decoration: none;
    color: #d10024;
  }
`;
export const Error = styled.p`
  color: red;
  text-align: left !important;
`;
export const Form = styled.form`
  display: block;
  margin-top: 0em;
`;
