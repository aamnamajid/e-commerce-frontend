import styled from "styled-components";

export const TopHeader = styled.div`
  background-color: rgb(30, 31, 41);
  color: white;
  padding-top: 2px;
  padding-bottom: 2px;
`;

export const TopHeaderContainer = styled.div`
  padding-right: 15px;
  padding-left: 15px;
  margin-right: auto;
  margin-left: auto;
`;
export const HeaderLinks = styled.ul`
  margin: 0px;
  padding: 0px;
  list-style: none;
  text-align: left !important;
`;

export const HeaderLinksItem = styled.li`
  display: inline-block;
  margin-right: 15px;
  font-size: 12px;
`;
