import styled from "styled-components";

export const Container = styled.div`
  display: block;
  position: absolute;
  bottom: 0;
  width: 100%;
  height: 2.5rem;
`;

export const FooterContainer = styled.div`
  background-color: #000;
  color: white;
  padding: 5px 0px;
`;
export const FooterText = styled.p`
  text-align: left 1important;
  padding-top: 0.5rem !important;
`;
