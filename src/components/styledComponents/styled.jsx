import styled from "styled-components";
import { mobile } from "../responsive";


// slider css
const SliderContainer = styled.div`
  width: 100%;
  height: 100vh;
  margin-top: 19px;
  display: flex;
  position: relative;
  overflow: hidden;
  ${mobile({ display: "none" })}
`;

const Container = styled.div`
  height: 60px;
  margin-bottom: 10px;
  ${mobile({ height: "50px" })}
`;





const Left = styled.div`
  flex: 1;
  display: flex;
  align-items: center;
`;

export const Text = styled.p`
display: inline-block;
margin-right: 15px;
font-size: 12px
`;

const Language = styled.span`
  font-size: 14px;
  cursor: pointer;
  ${mobile({ display: "none" })}
`;

const SearchContainer = styled.div`
  display: flex;
  align-items: center;
  margin-left: 20px;
  padding: 5px;
`;

const Center = styled.div`
  flex: 1;
  text-align: center;
`;

const Logo = styled.h1`
  font-weight: bold;
  ${mobile({ fontSize: "24px" })}
`;
const Right = styled.div`
  flex: 1;
  display: flex;
  align-items: center;
  justify-content: flex-end;
  @media (max-width: 768px) {
    overflow: hidden;
    flex-direction: column;
    max-height: ${({ isOpen }) => (isOpen ? "300px" : "0")};
    transition: max-height 0.3s ease-in;
    width: 100%;
  }
`;

const MenuItem = styled.div`
  font-size: 14px;
  cursor: pointer;
  margin-left: 25px;
  color: black;
  ${mobile({ fontSize: "12px", marginLeft: "10px" })}
`;
const Form = styled.form`
  display: flex;
  flex-direction: column;
`;

const Input = styled.input`
  flex: 1;
  min-width: 40%;
  margin: 10px 0;
  padding: 6px;
  border-radius: 30px 30px 30px 30px;
  border-color: red
`;

const Button = styled.button`
display: inline-block;
background: teal;
color: white;
font-size: 1em;
margin: 1em;
padding: 0.25em 1em;
border: 2px solid teal;
border-radius: 3px;
&:hover {
  text-decoration: none;
  color: white;
}
`

export {


    Center,
    Right,
    MenuItem,
    Logo,
    Input,
    Left,
    SearchContainer,
    Language,
    Form,
    Button,
    SliderContainer

}