import React, { useState, useEffect } from "react";
import { Link, useNavigate } from "react-router-dom";
import { SearchContainer, Input } from "./styledComponents/styled";
import { useSelector, useDispatch } from "react-redux";
import { logout } from "../actions/userActions";
import { NavDropdown, Row } from "react-bootstrap";
import AccountCircleIcon from "@mui/icons-material/AccountCircle";
import SearchIcon from "@mui/icons-material/Search";
import styled from "styled-components";
const Header = () => {
  const [search, setSearch] = useState("");
  const dispatch = useDispatch();
  const navigate = useNavigate();

  const [isOpen, setIsOpen] = useState(false);

  const [admin, setAdmin] = useState(false);

  const user = useSelector((state) => state.userLogin);
  const { userInfo } = user;

  useEffect(() => {
    if (userInfo) {
      if (userInfo.isAdmin) {
        setAdmin(true);
      }
    }
  }, [userInfo]);

  const logouthandler = () => {
    dispatch(logout());
    window.location.href = "/";
  };

  const searchHandler = () => {
    navigate(`/search/${search}`);
  };

  return (
    <>
      {!admin && (
        <Wrapper>
          <Nav>
            <Logo href="/">shop</Logo>
            <Hamburger onClick={() => setIsOpen(!isOpen)}>
              <span />
              <span />
              <span />
            </Hamburger>

            <Menu isOpen={isOpen}>
              <SearchContainer>
                <SearchIcon style={{ color: "gray", fontSize: 24 }} />
                <Input
                  placeholder="Search"
                  onChange={(e) => setSearch(e.target.value)}
                  onKeyPress={(event) => {
                    if (event.key === "Enter" && search) {
                      searchHandler();
                    } else {
                      navigate("/");
                    }
                  }}
                />
              </SearchContainer>
              <MenuLink href="/">HOME</MenuLink>
              <MenuLink href="/products">PRODUCTS</MenuLink>
              <MenuLink href="/cart">CART</MenuLink>

              {userInfo ? (
                <>
                  <StyledUl>
                    <DropDownLi>
                      <Dropbtn>
                        <AccountCircleIcon />
                      </Dropbtn>
                      <DropDownContent>
                        {" "}
                        <SubA href="/profile"> Profile</SubA>
                        <SubA href="/profile/myorders">My Orders</SubA>
                        <SubA onClick={logouthandler}>Logout</SubA>
                      </DropDownContent>
                    </DropDownLi>
                  </StyledUl>
                  {/* <NavDropdown
                  title={userInfo.name}
                  style={{ cursor: "pointer" }}
                >
                  <NavDropdown.Item href="/profile">Profile</NavDropdown.Item>
                  <NavDropdown.Item href="/profile/myorders">
                    My Orders
                  </NavDropdown.Item>

                  <NavDropdown.Item onClick={logouthandler}>
                    Logout
                  </NavDropdown.Item>
                </NavDropdown> */}
                </>
              ) : (
                <>
                  <MenuLink href="/register">REGISTER</MenuLink>
                  <MenuLink href="/login">LOGIN</MenuLink>
                </>
              )}
            </Menu>
          </Nav>
        </Wrapper>
      )}
    </>
  );
};

export default Header;

const MenuLink = styled.a`
  padding: 1rem;
  cursor: pointer;
  text-align: center;
  text-decoration: none;
  color: black;
  transition: all 0.3s ease-in;
  font-size: 0.9rem;
  &:hover {
    color: black;
    text-decoration: none;
  }
  @media (max-width: 768px) {
    color: white;
  }
`;

const Nav = styled.div`
  padding: 0 2rem;
  display: flex;
  justify-content: space-between;
  align-items: center;
  flex-wrap: wrap;
  background: white;
  position: absolute;
  top: 0;
  left: 0;
  right: 0;
  z-index: 2;
`;

const Logo = styled.a`
  padding: 1rem 0;
  color: black;
  text-decoration: none;
  font-weight: 800;
  font-size: 1.7rem;
  span {
    font-weight: 300;
    font-size: 1.3rem;
  }
  &:hover {
    text-decoration: none;
    color: black;
  }
`;

const Menu = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  position: relative;
  @media (max-width: 768px) {
    overflow: hidden;
    flex-direction: column;
    max-height: ${({ isOpen }) => (isOpen ? "340px" : "0")};
    transition: max-height 0.3s ease-in;
    width: 100%;
    background-color: rgb(30, 31, 41);
    color: white;
  }
`;

const Hamburger = styled.div`
  display: none;
  flex-direction: column;
  cursor: pointer;
  span {
    height: 2px;
    width: 25px;
    background: #d10024;
    margin-bottom: 4px;
    border-radius: 5px;
  }
  @media (max-width: 768px) {
    display: flex;
  }
`;
const Wrapper = styled.div`
  padding: 10px 20px 15px;
  display: flex;
  align-items: center;
  justify-content: space-between;
`;

// addi

const StyledUl = styled.ul`
  list-style-type: none;
  margin: 0;
  padding: 0;
  overflow: hidden;
  background-color: white;
`;

const StyledLi = styled.li`
  float: left;
`;

const Dropbtn = styled.div`
  display: inline-block;
  color: #d10024;
  text-align: center;
  padding: 14px 16px;
  text-decoration: none;
  &:hover {
    cursor: pointer;
  }
`;

const DropDownContent = styled.div`
  display: none;
  position: absolute;
  background-color: #f9f9f9;
  min-width: 160px;
  box-shadow: 0px 8px 16px 0px rgba(0, 0, 0, 0.2);
  z-index: 1;
  margin-left: -120px;
`;

const DropDownLi = styled(StyledLi)`
  display: inline-block;
  &:hover {
    cursor: pointer;
  }
  &:hover {
    background-color: white;
  }
  &:hover ${DropDownContent} {
    display: block;
  }
`;

const SubA = styled.a`
  color: black;
  padding: 12px 16px;
  text-decoration: none;
  display: block;
  text-align: left;
  &:hover {
    background-color: #f1f1f1;
    color: #d10024;
    text-decoration: none;
  }
`;
