import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { getAllProducts } from "../actions/productAction";
import { Left, Right } from "./styledComponents/styled";
import { Button } from "./styledComponents/global.styled";
import styled from "styled-components";
import { useParams } from "react-router";
import Paginate from "./Paginate";
import Rating from "./Rating";

const Wrapper = styled.div`
  padding: 10px 20px;
  margin-top: 50px;
  display: flex;
  align-items: center;
  justify-content: space-between;
  @media (max-width: 768px) {
    margin-top: 5px;
  }
`;

const Container = styled.div`
  display: flex;
  flex-wrap: wrap;
  flex-direction: row;
  justify-content: flex-start;
  align-items: flex-start;
  position: relative;
  padding: 0 40px;
`;

const Image = styled.img`
  height: 55%;
  z-index: 2;
`;

export const CardWrapper = styled.div`
  overflow: hidden;
  padding: 0 0 32px;
  margin: 48px auto 0;
  width: 18rem;
  height: 50vh;
  border: 1px solid #ececec;
`;

export const CardTitle = styled.div`
  font-size: 18px;
  font-family: Quicksand, arial, sans-serif;
`;

export const CardText = styled.div`
  font-size: 14px;
  font-family: Quicksand, arial, sans-serif;
`;

export const PContainer = styled.div`
  display: flex;
  justify-content: center;
`;
const FilterContainer = styled.div`
  display: flex;
  justify-content: space-between;
`;

const Filter = styled.div`
  margin: 10px;
`;

const FilterText = styled.span`
  font-size: 14px;
  font-weight: 600;
  margin-right: 20px;
`;

const Select = styled.select`
  padding: 6px;
  margin-right: 20px;
`;
const Option = styled.option``;

const AllProducts = () => {
  const { keyword } = useParams();
  const { pageNumber } = useParams() || 1;
  const [sort, setSort] = useState("newest");

  const dispatch = useDispatch();
  const allProducts = useSelector((state) => state.allProducts);
  const { error, products, pages, page } = allProducts;
  const [prods, setFilteredProducts] = useState(products);

  useEffect(() => {
    setFilteredProducts(products);
  }, [products]);

  useEffect(() => {
    dispatch(getAllProducts(keyword, pageNumber));
  }, [dispatch, keyword, pageNumber]);

  useEffect(() => {
    if (sort === "newest") {
      setFilteredProducts((prev) =>
        [...prev].sort((a, b) => a.createdAt - b.createdAt)
      );
    } else if (sort === "asc") {
      setFilteredProducts((prev) =>
        [...prev].sort((a, b) => a.price - b.price)
      );
    } else {
      setFilteredProducts((prev) =>
        [...prev].sort((a, b) => b.price - a.price)
      );
    }
  }, [sort]);
  if (!products) return <p>Error getting data</p>;
  return (
    <>
      <Wrapper>
        {error ? error : ""}
        <Left>Showing {prods.length} products</Left>
        <Right>
          <FilterContainer>
            <Filter>
              <FilterText>Sort Products:</FilterText>
              <Select onChange={(e) => setSort(e.target.value)}>
                <Option value="" disabled>
                  sort
                </Option>
                <Option value="asc">Price (asc)</Option>
                <Option value="desc">Price (desc)</Option>
              </Select>
            </Filter>
          </FilterContainer>
        </Right>
      </Wrapper>
      <Container>
        {prods.map((product) => (
          <CardWrapper key={product._id}>
            <Image src={product.prodImage} />
            <CardTitle>{product.name}</CardTitle>
            <CardText>
              <Rating value={product.rating} />
            </CardText>
            <CardText>Rs {product.price}</CardText>
            <Button as="a" href={`/product/${product._id}`}>
              {" "}
              View Product
            </Button>
          </CardWrapper>
        ))}
      </Container>
      <PContainer>
        <Paginate pages={pages} page={page} keyword={keyword ? keyword : ""} />
      </PContainer>
    </>
  );
};

export default AllProducts;
