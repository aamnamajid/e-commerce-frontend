import React from "react";
import {
  Container,
  FooterContainer,
  FooterText,
} from "./styledComponents/footer.styled";
const Footer = () => {
  return (
    <Container>
      <FooterContainer>
        <FooterText>© 2020 Copyright </FooterText>
      </FooterContainer>
    </Container>
  );
};

export default Footer;
