import {
  TopHeader,
  TopHeaderContainer,
  HeaderLinksItem,
  HeaderLinks,
} from "./styledComponents/topheader.styled";

const Announcement = () => {
  return (
    <TopHeader>
      <TopHeaderContainer>
        <HeaderLinks>
          <HeaderLinksItem>call: 0900 78601</HeaderLinksItem>
          <HeaderLinksItem>email: shop@shop.com</HeaderLinksItem>
        </HeaderLinks>
      </TopHeaderContainer>
    </TopHeader>
  );
};

export default Announcement;
