import React, { useState, useEffect } from "react";
import styled from "styled-components";
import Rating from "./Rating";
import { useSelector, useDispatch } from "react-redux";
import { Form } from "react-bootstrap";
import { addProductReview } from "../actions/productAction";
import {
  Title,
  FormGroup,
  FormLabel,
  Button,
} from "../components/styledComponents/global.styled";
const RevContainer = styled.div`
  margin: 50px;
`;
const Review = styled.div`
  font-size: 14px;
  padding-bottom: 10px;
  text-align: left !important;
  border-bottom: 2px solid rgb(108, 106, 116);
`;
const RevTitle = styled.p`
  margin-top: 20px;
  font-weight: 500;
  font-size: 14px;
  text-transform: capitalize;
`;

const Titl = styled(Title)`
  text-align: left !important;
`;

const Reviews = ({ rev, arr = [], productId }) => {
  const dispatch = useDispatch();

  const user = useSelector((state) => state.userLogin);
  const { userInfo } = user;

  const [message, setMessage] = useState("");
  const [rating, setRating] = useState(0);
  const [comment, setComment] = useState("");

  useEffect(() => {
    if (!userInfo) {
      setMessage("Please login to add review");
    }
  }, [userInfo]);

  const submitHandler = (e) => {
    dispatch(
      addProductReview(productId, {
        rating,
        comment,
      })
    );
  };
  return (
    <RevContainer>
      <Titl>Reviews ({rev})</Titl>
      {arr.map((rev) => (
        <Review>
          <RevTitle>{rev.name}</RevTitle>
          <Rating value={rev.rating} />
          <p>{rev.createdAt.substring(0, 10)}</p>
          <p>{rev.comment}</p>
        </Review>
      ))}

      <Titl>Add a Customer Review</Titl>
      <Form onSubmit={submitHandler}>
        <FormGroup controlId="rating">
          <FormLabel>Rating</FormLabel>
          <Form.Control
            as="select"
            value={rating}
            onChange={(e) => {
              setRating(e.target.value);
            }}
          >
            <option value="" disabled>
              Select
            </option>
            <option value="1">1</option>
            <option value="2">2</option>
            <option value="3">3</option>
            <option value="4">4</option>
            <option value="5">5</option>
          </Form.Control>
        </FormGroup>
        <FormGroup controlId="comment">
          <FormLabel>Comment</FormLabel>
          <Form.Control
            as="textarea"
            row="5"
            value={comment}
            onChange={(e) => setComment(e.target.value)}
          ></Form.Control>
        </FormGroup>
        {message ? message : <Button type="submit">Submit</Button>}
      </Form>
    </RevContainer>
  );
};

export default Reviews;
