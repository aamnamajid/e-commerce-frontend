import React from 'react'
import StarIcon from '@mui/icons-material/Star';
import StarHalfIcon from '@mui/icons-material/StarHalf';
import StarOutlineIcon from '@mui/icons-material/StarOutline';
import styled from 'styled-components'

const RatingComponent = styled.div`
    color: #FFC300
`

const Rating = ({value}) => {
    return (
        <RatingComponent>
            <span >
                { value >=1
                    ? <StarIcon/> 
                    : value >= 0.5
                    ? <StarHalfIcon/>
                    : <StarOutlineIcon/>}
               
            </span>
            <span>
                {
                    value >=2
                    ? <StarIcon/> 
                    : value >= 1.5
                    ? <StarHalfIcon/>
                    : <StarOutlineIcon/> 
                }
            </span>
            <span>
                {
                    value >=3
                    ? <StarIcon/> 
                    : value >= 2.5
                    ? <StarHalfIcon/>
                    : <StarOutlineIcon/> 
                }
            </span>
            <span>
                {
                    value >=4
                    ? <StarIcon/> 
                    : value >= 3.5
                    ? <StarHalfIcon/>
                    : <StarOutlineIcon/> 
                }
            </span>
            <span>
                {
                    value >=5
                    ? <StarIcon/> 
                    : value >= 4.5
                    ? <StarHalfIcon/>
                    : <StarOutlineIcon/> 
                }
            </span>
            
        </RatingComponent>
    )
}

export default Rating
