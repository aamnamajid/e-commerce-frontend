import React, { useEffect } from "react";
import { Link, useLocation, useNavigate, useParams } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { Row, Col, Image, ListGroup, Form, Card } from "react-bootstrap";
import { Button, Title } from "../components/styledComponents/global.styled";
import { addToCart, removeFromCart } from "../actions/cartActions";
import styled from "styled-components";
const Wrapper = styled.div`
  padding: 50px;
  display: flex;
  margin-top: 50px; ;
`;
const Cart = () => {
  const location = useLocation();
  const navigate = useNavigate();
  const { id } = useParams();
  const productId = id;

  const qty = location.search ? Number(location.search.split("=")[1]) : 1;

  const dispatch = useDispatch();

  const cartItems = useSelector((state) => state.cart);
  const { orderItems } = cartItems;

  useEffect(() => {
    if (productId) {
      dispatch(addToCart(productId, qty));
    }
  }, [dispatch, productId, qty]);

  const removeFromCartHandler = (id) => {
    dispatch(removeFromCart(id));
  };

  const checkoutHandler = () => {
    navigate("/order/shipping");
  };

  return (
    <Wrapper>
      <Row className=" container">
        <Col md={8}>
          <Title>Shopping Cart</Title>
          {orderItems.length === 0 ? (
            " Your cart is empty"
          ) : (
            <ListGroup variant="flush">
              {orderItems.map((item) => (
                <ListGroup.Item key={item.productID}>
                  <Row>
                    <Col md={2}>
                      <Image src={item.image} alt={item.name} fluid rounded />
                    </Col>
                    <Col md={3}>
                      <Link to={`/product/${item.productID}`}>{item.name}</Link>
                    </Col>
                    <Col md={2}>Rs {item.price}</Col>
                    <Col md={2}>
                      <Form.Control
                        as="select"
                        value={item.qty}
                        onChange={(e) =>
                          dispatch(
                            addToCart(item.productID, Number(e.target.value))
                          )
                        }
                      >
                        {[...Array(item.countInStock).keys()].map((x) => (
                          <option key={x + 1} value={x + 1}>
                            {x + 1}
                          </option>
                        ))}
                      </Form.Control>
                    </Col>
                    <Col md={2}>
                      <Button
                        onClick={() => removeFromCartHandler(item.productID)}
                      >
                        Remove
                      </Button>
                    </Col>
                  </Row>
                </ListGroup.Item>
              ))}
            </ListGroup>
          )}
        </Col>
        <Col md={4}>
          <Card>
            <ListGroup variant="flush">
              <ListGroup.Item>
                <h2>
                  Subtotal (
                  {orderItems.reduce((acc, item) => acc + item.qty, 0)}) items
                </h2>
                Rs{" "}
                {orderItems
                  .reduce((acc, item) => acc + item.qty * item.price, 0)
                  .toFixed(2)}
              </ListGroup.Item>
              <ListGroup.Item>
                <Button
                  type="button"
                  disabled={orderItems.length === 0}
                  onClick={checkoutHandler}
                >
                  Proceed To Checkout
                </Button>
              </ListGroup.Item>
            </ListGroup>
          </Card>
        </Col>
      </Row>
    </Wrapper>
  );
};

export default Cart;
