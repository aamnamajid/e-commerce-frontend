import React from "react";
import Slider from "../components/Slider.js";
import TopProducts from "../components/TopProducts.js";
import Categories from "../components/Categories.js";
import styled from "styled-components";

export const Container = styled.div`
  @media (max-width: 768px) {
    margin-top: 20px;
  }
`;
const HomeScreen = () => {
  return (
    <Container>
      <Slider />
      <Categories />
      <TopProducts />
    </Container>
  );
};

export default HomeScreen;
