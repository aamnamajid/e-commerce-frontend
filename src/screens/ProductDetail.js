import React, { useState, useEffect } from "react";
import { useParams, useNavigate } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { getProductDetail } from "../actions/productAction";
import { ListGroup, Row, Col, Form } from "react-bootstrap";
import styled from "styled-components";
import { mobile } from "../components/responsive";
import { Button } from "../components/styledComponents/global.styled";
import Reviews from "../components/Reviews";

const Wrapper = styled.div`
  padding: 50px;
  display: flex;
  margin-top: 50px;
  ${mobile({ padding: "10px", flexDirection: "column" })};
`;

const ImgContainer = styled.div`
  flex: 1;
`;

const Image = styled.img`
  width: 100%;
  height: 70vh;
  ${mobile({ height: "40vh", width: "100%" })}
`;

const InfoContainer = styled.div`
  flex: 1;
  padding: 0px 50px;
  ${mobile({ padding: "10px" })}
`;

const Title = styled.p`
  font-size: 24px;
  font-weight: 500;
  text-align: left !important;
`;

const Desc = styled.p`
  margin-top: 49px;
  margin-bottom: 49px;
  font-size: 16px;
  text-align: left !important;
`;

const Price = styled.p`
  font-weight: 500;
  font-size: 16px;
  text-align: left !important;
  color: rgb(108, 106, 116);
`;
const InStock = styled.p`
  font-weight: 500;
  font-size: 16px;
  text-align: left !important;
  color: green;
`;
const OutOfStock = styled.p`
  font-weight: 500;
  font-size: 16px;
  text-align: left !important;
  color: red;
`;

const ProductDetail = () => {
  const { id } = useParams();
  const [qty, setQty] = useState(1);

  const dispatch = useDispatch();
  const navigate = useNavigate();

  const detailedProduct = useSelector((state) => state.productDetails);
  const { products } = detailedProduct;
  const { reviews } = products;
  const user = useSelector((state) => state.userLogin);
  const { userInfo } = user;

  useEffect(() => {
    dispatch(getProductDetail(id));
  }, [id, dispatch]);

  const addToCartHandler = () => {
    navigate(`/cart/${id}?qty=${qty}`);
  };
  return (
    <>
      <Wrapper>
        <ImgContainer>
          <Image src={products.prodImage} alt=" specific product" />
        </ImgContainer>

        <InfoContainer>
          <Title>{products.name}</Title>
          <Price>Rs {products.price}</Price>
          {products.countInStock > 0 ? (
            <InStock>In Stock</InStock>
          ) : (
            <OutOfStock>Out Of Stock</OutOfStock>
          )}

          <Desc>{products.description}</Desc>
          {products.countInStock > 0 && (
            <ListGroup.Item>
              <Row>
                <Col>Qty</Col>
                <Col>
                  <Form.Control
                    as="select"
                    value={qty}
                    onChange={(e) => {
                      setQty(e.target.value);
                    }}
                  >
                    {[...Array(products.countInStock).keys()].map((x) => (
                      <option key={x + 1} value={x + 1}>
                        {" "}
                        {x + 1}
                      </option>
                    ))}
                  </Form.Control>
                </Col>
              </Row>
            </ListGroup.Item>
          )}
          {userInfo ? (
            <Button onClick={addToCartHandler}>Add to Cart</Button>
          ) : (
            <Button style={{ backgroundColor: "grey" }}>Add to Cart</Button>
          )}
        </InfoContainer>
      </Wrapper>

      <Reviews rev={products.numReviews} arr={reviews} productId={id} />
    </>
  );
};

export default ProductDetail;
