import React, { useEffect } from "react";
import { useFormik } from "formik";
import { useSelector, useDispatch } from "react-redux";
import { login } from "../actions/userActions";
import {
  Container,
  Row,
  Col,
  FormSVG,
  FormGroup,
  FormLabel,
  Input,
  Title,
  Button,
  LinkPage,
  Error,
  Form,
} from "../components/styledComponents/global.styled";

const validate = (values) => {
  const errors = {};

  if (!values.password) {
    errors.password = "* Password is required";
  } else if (values.password.length < 6) {
    errors.password = "Must be 6 characters or greater";
  }

  if (!values.email) {
    errors.email = "* Email is required";
  } else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)) {
    errors.email = "Invalid email address";
  }

  return errors;
};

const LoginScreen = () => {
  const dispatch = useDispatch();
  const user = useSelector((state) => state.userLogin);
  const { userInfo, error } = user;

  useEffect(() => {
    if (userInfo) {
      if (userInfo.isAdmin) {
        window.location.href = "/admin";
      } else {
        window.location.href = "/";
      }
      //window.location.href = "/";
    }
  }, [userInfo]);

  const formik = useFormik({
    initialValues: {
      password: "",
      email: "",
    },
    validate,
    onSubmit: async (values) => {
      const { email, password } = values;
      dispatch(login(email, password));
    },
  });

  return (
    <Container>
      <Row>
        <Col size={6}>
          <FormSVG
            src="img/register.svg"
            alt="signup"
            style={{ marginTop: "75px" }}
          />
        </Col>
        <Col size={6}>
          <Title>SIGN IN</Title>

          <Form onSubmit={formik.handleSubmit}>
            {error && <Error>{error}</Error>}
            <FormGroup>
              <FormLabel htmlFor="email">Email Address</FormLabel>
              <Row>
                <Input
                  id="email"
                  name="email"
                  type="email"
                  onChange={formik.handleChange}
                  value={formik.values.email}
                />
              </Row>
              {formik.errors.email ? (
                <Error>{formik.errors.email}</Error>
              ) : null}
            </FormGroup>

            <FormGroup>
              <FormLabel htmlFor="password">Password</FormLabel>
              <Input
                id="password"
                name="password"
                type="password"
                onChange={formik.handleChange}
                value={formik.values.password}
              />
              {formik.errors.password ? (
                <Error>{formik.errors.password}</Error>
              ) : null}
            </FormGroup>

            <Button type="submit">Login</Button>
            <div className="mt-5">
              Did not have an account?{" "}
              <LinkPage to="/register">Register</LinkPage>
            </div>
          </Form>
        </Col>
      </Row>
    </Container>
  );
};

export default LoginScreen;
