import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useParams } from "react-router-dom";
import { getOrderDetails } from "../actions/orderAction";
import styled from "styled-components";
import { mobile } from "../components/responsive";
import { ORDER_PAY_RESET } from "../constants/orderConstants";

export const ListGroup = styled.div`
  display: flex;
  flex-direction: column;
  padding-left: 0;
  margin-bottom: 0;
`;
export const ListItem = styled.div`
  position: relative;
  display: block;
  padding: 0.75rem 1.25rem;
  margin-bottom: -1px;
  background-color: #fff;
  border-top: 1px solid rgba(0, 0, 0, 0.125);
  border-bottom: 1px solid rgba(0, 0, 0, 0.125);
`;
export const Row = styled.div`
  display: flex;

  @media (max-width: 768px) {
    flex-direction: column;
  }
`;
export const Left = styled.div`
  text-align: left !important;
`;
export const Col = styled.div`
  flex: ${(props) => props.size};
  @media (max-width: 768px) {
    margin-right: 0px;
  }
`;

const Container = styled.div`
  padding: 0 30px;
  margin-top: 50px;
  ${mobile({ padding: "10px", flexDirection: "column" })};
`;

export const Image = styled.img`
  width: 40px;
  height: 40px;
`;
export const Title = styled.h4`
  margin-bottom: 20px;
`;
export const Text = styled.p`
  justify-content: left;
`;
export const Box = styled.div`
  border-top: 3px solid #ececec;
  padding: 12px;
`;
const OrderScreen = () => {
  const { orderId } = useParams();
  const dispatch = useDispatch();

  const orderDetails = useSelector((state) => state.orderDetails);
  const { order } = orderDetails;

  const orderPay = useSelector((state) => state.payOrder);
  const { success } = orderPay;

  const [loading, setLoading] = useState(false);

  useEffect(() => {
    if (!order || order._id !== orderId || success) {
      dispatch({ type: ORDER_PAY_RESET });
      dispatch(getOrderDetails(orderId));
    }
  }, [dispatch, orderId, success, order]);

  if (loading) {
    return (
      <div>
        <p>Loading...</p>
      </div>
    );
  } else {
    return (
      <Container>
        <Left>
          <Title>Your order is confirmed!</Title>
          <p>Hi! your order has been confirmed and will be shipping soon.</p>
        </Left>
        {order.user && (
          <>
            <Box>
              <Row>
                <Col size={4}>
                  <h6>Order Date</h6>
                </Col>
                <Col size={4}>
                  <h6>Payment Method</h6>
                </Col>
                <Col size={4}>
                  <h6>Address</h6>
                </Col>
              </Row>
              <Row>
                <Col size={4}>
                  <p> {order.createdAt.substring(0, 10)}</p>
                </Col>
                <Col size={4}>
                  <p>Cash on Delivery</p>
                </Col>
                <Col size={4}>
                  <p>{order.shippingAddress.address}</p>
                </Col>
              </Row>
            </Box>
            <Box>
              <Title>Order Items</Title>

              {order.orderItems &&
                order.orderItems.map((item, index) => (
                  <>
                    <Row>
                      <Col size={3}>
                        <Image
                          style={{ width: "50px", height: "50px" }}
                          src={item.image}
                          alt={item.name}
                          fluid
                          rounded
                        />
                      </Col>
                      <Col size={3}>
                        <p>{item.name}</p>
                      </Col>
                      <Col size={3}>Qty{item.qty}</Col>
                      <Col size={3}>Rs{item.qty * item.price}</Col>
                    </Row>
                  </>
                ))}
            </Box>
            <Box>
              <Title>Order Summary</Title>

              {order && (
                <>
                  <Row>
                    <Col size={2}>SubTotal</Col>
                    <Col size={2}></Col>
                    <Col size={2}>Rs {order.itemsPrice}</Col>
                  </Row>
                  <Row>
                    <Col size={2}>Shipping</Col>
                    <Col size={2}></Col>
                    <Col size={2}>Rs {order.shippingPrice}</Col>
                  </Row>
                  <Row>
                    <Col size={2}>Tax</Col>
                    <Col size={2}></Col>
                    <Col size={2}>Rs {order.taxPrice}</Col>
                  </Row>
                </>
              )}
            </Box>
            <Box>
              <Row>
                <Col size={2}>Total</Col>
                <Col size={2}></Col>
                <Col size={2}>Rs {order.totalPrice}</Col>
              </Row>
            </Box>
            <Box>
              <Left>
                <p>Hope you enjoy your purchase</p>
                <h6>Thank You</h6>
              </Left>
            </Box>
          </>
        )}
      </Container>
    );
  }
};

export default OrderScreen;
