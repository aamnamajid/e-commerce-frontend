import React,{useEffect} from 'react'
import {useDispatch, useSelector} from 'react-redux'
import { useFormik } from 'formik'
import {
  Container,
  Row,
  Col,
  FormSVG,
  FormGroup,
  FormLabel,
  Input,
  Title,
  Button,
  LinkPage,
  Error,
  Form,
} from "../components/styledComponents/global.styled";
import { register } from '../actions/userActions';

const validate = values => {
  const errors = {};

  if (!values.name) {
    errors.name = '* Name is required';
  } else if (values.name.length > 30) {
    errors.name = 'Must be 30 characters or less';
  }

  if (!values.password) {
    errors.password = '* Password is required';
  } else if (values.password.length < 6) {
    errors.password = 'Must be 6 characters or greater';
  }

  if (!values.email) {
    errors.email = '* Email is required';
  } else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)) {
    errors.email = 'Invalid email address';
  }

  return errors
}


const RegisterUser = () => {
 
  const dispatch = useDispatch()
  const user = useSelector(state=> state.userRegister)
  const {error, userInfo} = user

  useEffect(() => {
    if(userInfo){
      window.location.href="/login"
    }
  }, [userInfo])

  const formik = useFormik({
    initialValues: {
      name: '',
      password: '',
      email: ''

    },
    validate,
    onSubmit: async (values) => {
      const {name, email, password} = values
      dispatch(register(name,email,password))

    
    },
  });

  return (
    <Container>

    <Row>
      <Col size={6}>
             <FormSVG src="img/log.svg" alt="logo" />

      </Col>
      <Col size={6}>
        <Title>SIGN UP</Title>

     
        <Form onSubmit={formik.handleSubmit} >
        {error && (
            <Error>{error}</Error>
          )}
      
          <FormGroup>
            <FormLabel htmlFor="name">Name</FormLabel>
            <Input
              id="name"
              name="name"
              type="text"
              onChange={formik.handleChange}
              value={formik.values.name}
            />
            {formik.errors.name ? <Error>{formik.errors.name}</Error> : null}

          </FormGroup>
          <FormGroup>
            <FormLabel htmlFor="email">Email Address</FormLabel>
            <Input
              id="email"
              name="email"
              type="email"
              onChange={formik.handleChange}
              value={formik.values.email}
            />
            {formik.errors.email ? <Error>{formik.errors.email}</Error> : null}

          </FormGroup>




          <FormGroup>
            <FormLabel htmlFor="password">Password</FormLabel>
            <Input
              id="password"
              name="password"
              type="password"
              onChange={formik.handleChange}
              value={formik.values.password}
            />
            {formik.errors.password ? <Error>{formik.errors.password}</Error> : null}

          </FormGroup>



        
          <Button type='submit' >Register</Button>


          <div className='mt-5'>
          Already have an account? <LinkPage to='/login'>Login</LinkPage>
      
          </div>
        </Form>
      </Col>
    </Row>
  </Container>
  );

}

export default RegisterUser
