import React, { useEffect, useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import styled from "styled-components";
import { Form } from "react-bootstrap";
import { Button } from "./components/styledcomponents/global.styled";
import { createProductByAdmin } from "../../actions/productAction";
export const Grid = styled.div`
  padding-left: 80px;
  padding-right: 80px;
  text-align: left !important;
`;
export const Row = styled.div`
  display: flex;
  text-align: left !important;
  @media (max-width: 550px) {
    flex-direction: column;
  }
`;

export const Col = styled.div`
  flex: ${(props) => props.size};
  margin: 15px;
`;
const Title = styled.h4`
  font-weight: bold;
  text-align: left !important;
  margin-top: 30px;
  margin-bottom: 20px;
`;

const CreateProductScreen = () => {
  const dispatch = useDispatch();
  const productCreated = useSelector((state) => state.createProduct);
  const { success } = productCreated;

  const [name, setName] = useState("");
  const [category, setCategory] = useState("");
  const [code, setCode] = useState("");
  const [price, setPrice] = useState(0);
  const [description, setDescription] = useState("");
  const [countInStock, setCountInStock] = useState(0);
  const [prodImage, setImage] = useState(null);

  useEffect(() => {
    if (success) {
      window.location.href = "/admin/product";
    }
  }, [success]);

  const submitHandler = (e) => {
    e.preventDefault();
    const data = new FormData();
    data.append("name", name);
    data.append("category", category);
    data.append("code", code);
    data.append("price", price);
    data.append("description", description);
    data.append("countInStock", countInStock);
    data.append("prodImage", prodImage);
    dispatch(createProductByAdmin(data));
  };

  return (
    <Grid>
      <Title>Create Product</Title>
      <Form onSubmit={submitHandler}>
        <Form.Group controlId="name">
          <Form.Label>Product Name</Form.Label>
          <Form.Control
            type="name"
            placeholder="Enter name"
            value={name}
            onChange={(e) => setName(e.target.value)}
          ></Form.Control>
        </Form.Group>
        <Form.Group controlId="code">
          <Form.Label>Product Code</Form.Label>
          <Form.Control
            type="code"
            placeholder="Enter Code"
            value={code}
            onChange={(e) => setCode(e.target.value)}
          ></Form.Control>
        </Form.Group>

        <Form.Group controlId="category">
          <Form.Label>Product Category</Form.Label>
          <Form.Control
            as="select"
            placeholder="Enter category"
            value={category}
            onChange={(e) => setCategory(e.target.value)}
          >
            <option value="" disabled>
              [Please select one]
            </option>
            <option value="mobile">Mobile</option>
            <option value="camera">Camera</option>
            <option value="laptop">Laptop</option>
          </Form.Control>
        </Form.Group>
        <Form.Group controlId="price">
          <Form.Label>Price</Form.Label>
          <Form.Control
            type="number"
            value={price}
            onChange={(e) => setPrice(e.target.value)}
          ></Form.Control>
        </Form.Group>
        <Form.Group controlId="countInStock">
          <Form.Label>Count In Stock</Form.Label>
          <Form.Control
            type="number"
            value={countInStock}
            onChange={(e) => setCountInStock(e.target.value)}
          ></Form.Control>
        </Form.Group>
        <Form.Group controlId="description">
          <Form.Label>Description</Form.Label>
          <Form.Control
            as="textarea"
            row="5"
            value={description}
            onChange={(e) => setDescription(e.target.value)}
          ></Form.Control>
        </Form.Group>
        <Form.Group controlId="prodImage">
          <Form.Label>Upload Image</Form.Label>
          <Form.Control
            type="file"
            onChange={(e) => setImage(e.target.files[0])}
          ></Form.Control>
        </Form.Group>

        <Button type="submit">Create</Button>
      </Form>
    </Grid>
  );
};

export default CreateProductScreen;
