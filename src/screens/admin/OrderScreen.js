import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import ClearIcon from "@mui/icons-material/Clear";
import DeleteIcon from "@mui/icons-material/Delete";
import CheckIcon from "@mui/icons-material/Check";
import { CDataTable, CButton, CFormText } from "@coreui/react";
import { deleteOrder, getAllOrdersAdmin } from "../../actions/orderAction";
import {
  Container,
  Title,
  Error,
} from "./components/styledcomponents/global.styled";

const OrderScreen = () => {
  const dispatch = useDispatch();

  const ordersList = useSelector((state) => state.allOrders);
  const {
    error,
    orders: { orders },
  } = ordersList;

  useEffect(() => {
    dispatch(getAllOrdersAdmin());
  }, [dispatch]);

  const fields = [
    { key: "_id", _style: { width: "5%" } },
    { key: "totalPrice", _style: { width: "20%" } },
    { key: "isPaid", label: "Paid", _style: { width: "20%" } },
    { key: "isDelievered", label: "Delievered", _style: { width: "20%" } },
    { key: "createdAt", _style: { width: "20%" } },
    {
      key: "delete",
      label: "",
      _style: { width: "1%" },
      sorter: false,
      filter: false,
    },
    {
      key: "show_details",
      label: "",
      _style: { width: "1%" },
      sorter: false,
      filter: false,
    },
  ];

  const handleDelete = (id) => {
    dispatch(deleteOrder(id));
  };
  return (
    <Container>
      <Title>Orders List</Title>
      {error ? <Error>Error displaying orders</Error> : ""}

      <CDataTable
        items={orders}
        fields={fields}
        columnFilter
        tableFilter
        footer
        itemsPerPageSelect
        itemsPerPage={8}
        hover
        sorter
        pagination
        scopedSlots={{
          delete: (item, index) => {
            return (
              <td className="py-2">
                <CButton
                  color="danger"
                  size="sm"
                  onClick={() => {
                    handleDelete(item._id);
                  }}
                >
                  <DeleteIcon />
                </CButton>
              </td>
            );
          },
          createdAt: (item, index) => {
            return (
              <td className="py-2">
                <CFormText>{item.createdAt.substring(0, 10)}</CFormText>
              </td>
            );
          },
          isPaid: (item, index) => {
            return (
              <td className="py-2">
                {item.isPaid ? <CheckIcon /> : <ClearIcon />}
              </td>
            );
          },
          isDelievered: (item, index) => {
            return (
              <td className="py-2">
                {item.isDelievered ? <CheckIcon /> : <ClearIcon />}
              </td>
            );
          },
          show_details: (item, index) => {
            return (
              <td className="py-2">
                <CButton
                  color="primary"
                  variant="outline"
                  shape="square"
                  size="sm"
                  onClick={() => {
                    window.location.href = `/admin/orders/${item._id}`;
                  }}
                >
                  Details
                </CButton>
              </td>
            );
          },
        }}
      />
    </Container>
  );
};

export default OrderScreen;
