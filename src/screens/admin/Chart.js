import React, { useEffect } from "react";
import { Line } from "react-chartjs-2";
import { useDispatch, useSelector } from "react-redux";
import { getAllOrdersAdmin } from "../../actions/orderAction";
import { Error } from "./components/styledcomponents/global.styled";

const options = {
  scales: {
    y: {
      beginAtZero: true,
      ticks: {
        stepSize: 1,
      },
      max: 20,
      min: 0,
    },
  },
};

const Chart = () => {
  const dispatch = useDispatch();
  const ordersList = useSelector((state) => state.allOrders);
  const {
    error,
    orders: { orders },
  } = ordersList;
  var totalOrdersPerDay = [];

  if (orders) {
    var dates = [...new Set(orders.map((p) => p.createdAt.substring(0, 10)))];
    let count = 0;
    for (let i = 0; i < dates.length; i++) {
      for (let j = 0; j < orders.length; j++) {
        if (dates[i] === orders[j].createdAt.substring(0, 10)) {
          count++;
        }
      }
      totalOrdersPerDay.push(count);
      count = 0;
    }
  }

  const data = {
    labels: dates,
    datasets: [
      {
        label: "# of Orders",
        data: totalOrdersPerDay,
        fill: false,
        backgroundColor: "rgb(255, 99, 132)",
        borderColor: "rgba(255, 99, 132, 0.2)",
      },
    ],
  };
  useEffect(() => {
    dispatch(getAllOrdersAdmin());
  }, [dispatch]);
  return (
    <div style={{ marginTop: "30px", padding: "0 20px" }}>
      {error ? <Error>Error displaying chart</Error> : ""}
      <h4>Daily Orders</h4>
      <Line data={data} options={options} />
    </div>
  );
};

export default Chart;
