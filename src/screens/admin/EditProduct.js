import React, { useEffect, useState } from "react";
import { useParams } from "react-router";
import { useSelector, useDispatch } from "react-redux";
import { Button } from "../../components/styledComponents/global.styled";
import styled from "styled-components";
import { Form } from "react-bootstrap";
import { getProductDetail, updateProduct } from "../../actions/productAction";

export const Grid = styled.div`
  padding-left: 80px;
  padding-right: 80px;
`;
const Btn = styled(Button)`
  margin-right: 340px;
`;

export const Row = styled.div`
  display: flex;
  padding-right: 300px;
  text-align: left !important;
  @media (max-width: 550px) {
    flex-direction: column;
  }
`;

export const Col = styled.div`
  flex: ${(props) => props.size};
  margin: 15px;
`;
const Title = styled.h4`
  font-weight: bold;
  text-align: left !important;
  margin-top: 30px;
  margin-bottom: 20px;
`;

const Message = styled.p`
  font-weight: bold;
  background-color: green;
  color: white;
`;

const EditProduct = () => {
  const { id } = useParams();
  const dispatch = useDispatch();

  const detailedProduct = useSelector((state) => state.productDetails);
  const { error, products } = detailedProduct;

  const updateProd = useSelector((state) => state.updateProduct);
  const { success } = updateProd;

  const [name, setName] = useState("");
  const [code, setCode] = useState("");
  const [description, setDesc] = useState("");
  const [category, setCategory] = useState("");
  const [price, setPrice] = useState(0);
  const [countInStock, setCountInStock] = useState(0);

  const [message, setMessage] = useState(null);

  useEffect(() => {
    if (success) {
      setMessage("Product Updated Successfully");
      window.location.href = "/admin/product";
    }
  }, [success]);

  useEffect(() => {
    if (!products.name || !products.id === id) {
      dispatch(getProductDetail(id));
    } else {
      setName(products.name);
      setCode(products.code);
      setPrice(products.price);
      setCategory(products.category);
      setDesc(products.description);
      setCountInStock(products.countInStock);
    }
  }, [dispatch, id, products]);

  const submitHandler = (e) => {
    e.preventDefault();
    dispatch(
      updateProduct({
        _id: id,
        name,
        code,
        price,
        category,
        description,
        countInStock,
      })
    );
  };

  return (
    <Grid>
      <Title> Edit Product {products._id}</Title>
      <Message>{error ? error : ""}</Message>
      <Message>{message ? message : ""}</Message>
      <Form onSubmit={submitHandler}>
        <Row>
          <Col size={6}>
            <Form.Group controlId="name">
              <Form.Label>Name</Form.Label>
              <Form.Control
                type="name"
                placeholder="Enter name"
                value={name}
                onChange={(e) => setName(e.target.value)}
              ></Form.Control>
            </Form.Group>
          </Col>
        </Row>
        <Row>
          <Col size={6}>
            <Form.Group controlId="code">
              <Form.Label>Product Code</Form.Label>
              <Form.Control
                type="code"
                placeholder="Enter code"
                value={code}
                onChange={(e) => setCode(e.target.value)}
              ></Form.Control>
            </Form.Group>
          </Col>
        </Row>
        <Row>
          <Col size={6}>
            <Form.Group controlId="price">
              <Form.Label>Product Price</Form.Label>
              <Form.Control
                type="number"
                placeholder="Enter Price"
                value={price}
                onChange={(e) => setPrice(e.target.value)}
              ></Form.Control>
            </Form.Group>
          </Col>
        </Row>
        <Row>
          <Col size={6}>
            <Form.Group controlId="category">
              <Form.Label>Product Category</Form.Label>
              <Form.Control
                type="text"
                placeholder="Enter Category"
                value={category}
                onChange={(e) => setCategory(e.target.value)}
              ></Form.Control>
            </Form.Group>
          </Col>
        </Row>
        <Row>
          <Col size={6}>
            <Form.Group controlId="description">
              <Form.Label>Product Description</Form.Label>
              <Form.Control
                as="textarea"
                row="5"
                value={description}
                onChange={(e) => setDesc(e.target.value)}
              ></Form.Control>
            </Form.Group>
          </Col>
        </Row>
        <Row>
          <Col size={6}>
            <Form.Group controlId="countInStock">
              <Form.Label>Product Count In Stock</Form.Label>
              <Form.Control
                type="number"
                placeholder="Enter stock count"
                value={countInStock}
                onChange={(e) => setCountInStock(e.target.value)}
              ></Form.Control>
            </Form.Group>
          </Col>
        </Row>

        <Btn type="submit">Update</Btn>
      </Form>
    </Grid>
  );
};

export default EditProduct;
