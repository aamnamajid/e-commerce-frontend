import React from "react";
import Infocard from "./components/Infocard";
import Chart from "./Chart";
const AdminScreen = () => {
  return (
    <>
      <Infocard />
      <Chart />
    </>
  );
};

export default AdminScreen;
