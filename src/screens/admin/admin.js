import React, { Component } from "react";
import { Routes, Route } from "react-router-dom";
import AdminScreen from "./AdminScreen";
import UserList from "./components/UserList";
import styled from "styled-components";
import Sidebar from "./components/Sidebar";
import EditUser from "./EditUser";
import ProductScreen from "./ProductScreen";
import CreateProductScreen from "./CreateProductScreen";
import CreateUser from "./CreateUser";
import EditProduct from "./EditProduct";
import OrderScreen from "./OrderScreen";
import OrderDetail from "./OrderDetail";
export const SideBarContainer = styled.div`
  display: flex;
`;
export const Wrapper = styled.div`
  flex: 4;
`;
export default class Admin extends Component {
  render() {
    return (
      <SideBarContainer>
        <Sidebar />
        <Wrapper>
          <Routes>
            <Route exact path="/" element={<AdminScreen />} />
            <Route path="/users" element={<UserList />} />
            <Route path="/users/:id/edit" element={<EditUser />} />
            <Route path="/users/create" element={<CreateUser />} />
            <Route path="/product" element={<ProductScreen />} />
            <Route path="/product/create" element={<CreateProductScreen />} />
            <Route path="/product/:id/edit" element={<EditProduct />} />
            <Route path="/orders" element={<OrderScreen />} />
            <Route path="/orders/:orderId" element={<OrderDetail />} />
          </Routes>
        </Wrapper>
      </SideBarContainer>
    );
  }
}
