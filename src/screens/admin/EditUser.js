import React, { useEffect, useState } from "react";
import { useParams } from "react-router";
import { useSelector, useDispatch } from "react-redux";
import { Title, Message } from "./components/styledcomponents/global.styled";
import {
  Row,
  Col,
  Btn,
  Grid,
} from "./components/styledcomponents/edituser.styled";
import { Form } from "react-bootstrap";
import { getUserDetail, updateUserAdmin } from "../../actions/userActions";

const EditUser = () => {
  const { id } = useParams();
  const dispatch = useDispatch();

  const userDetails = useSelector((state) => state.userDetails);
  const { error, user } = userDetails;

  const updateUser = useSelector((state) => state.updateUser);
  const { success } = updateUser;

  const [name, setName] = useState("");
  const [email, setEmail] = useState("");
  const [message, setMessage] = useState(null);

  useEffect(() => {
    if (success) {
      setMessage("Profile Updated Successfully");
      window.location.href = "/admin/users";
    }
  }, [success]);

  useEffect(() => {
    if (!user.name || user._id !== id) {
      dispatch(getUserDetail(id));
    } else {
      setName(user.name);
      setEmail(user.email);
    }
  }, [dispatch, id, user]);

  const submitHandler = (e) => {
    e.preventDefault();
    dispatch(updateUserAdmin({ _id: id, name, email }));
  };

  return (
    <Grid>
      <Title> Edit User Profile</Title>
      <Message>{error ? error : ""}</Message>
      <Message>{message ? message : ""}</Message>
      <Form onSubmit={submitHandler}>
        <Row>
          <Col size={6}>
            <Form.Group controlId="name">
              <Form.Label>Name</Form.Label>
              <Form.Control
                type="name"
                placeholder="Enter name"
                value={name}
                onChange={(e) => setName(e.target.value)}
              ></Form.Control>
            </Form.Group>
          </Col>
        </Row>
        <Row>
          <Col size={6}>
            <Form.Group controlId="email">
              <Form.Label>Email Address</Form.Label>
              <Form.Control
                type="email"
                placeholder="Enter email"
                value={email}
                onChange={(e) => setEmail(e.target.value)}
              ></Form.Control>
            </Form.Group>
          </Col>
        </Row>

        <Btn type="submit">Update</Btn>
      </Form>
    </Grid>
  );
};

export default EditUser;
