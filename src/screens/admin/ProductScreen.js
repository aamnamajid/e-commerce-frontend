import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import DeleteIcon from "@mui/icons-material/Delete";
import { CDataTable, CCardBody, CCollapse, CButton, CImg } from "@coreui/react";
import {
  deleteProduct,
  getAllProductsAdmin,
} from "../../actions/productAction";
import Rating from "../../components/Rating";
import { Row, Col } from "react-bootstrap";
import {
  Container,
  Title,
  Button,
  Details,
  Error,
} from "./components/styledcomponents/global.styled";

const ProductScreen = () => {
  const dispatch = useDispatch();

  const allProducts = useSelector((state) => state.getProducts);
  const { error, products } = allProducts;

  useEffect(() => {
    dispatch(getAllProductsAdmin());
  }, [dispatch]);
  const [details, setDetails] = useState([]);

  const toggleDetails = (index) => {
    const position = details.indexOf(index);
    let newDetails = details.slice();
    if (position !== -1) {
      newDetails.splice(position, 1);
    } else {
      newDetails = [...details, index];
    }
    setDetails(newDetails);
  };

  const fields = [
    { key: "prodImage", _style: { width: "5%" } },
    { key: "name", _style: { width: "15%" } },
    { key: "price", _style: { width: "10%" } },
    { key: "category", _style: { width: "15%" } },
    { key: "code", _style: { width: "2%" } },
    { key: "rating", _style: { width: "18%" } },
    {
      key: "delete",
      label: "",
      _style: { width: "1%" },
      sorter: false,
      filter: false,
    },
    {
      key: "show_details",
      label: "",
      _style: { width: "1%" },
      sorter: false,
      filter: false,
    },
    {
      key: "edit",
      label: "",
      _style: { width: "1%" },
      sorter: false,
      filter: false,
    },
  ];

  const handleDelete = (id) => {
    dispatch(deleteProduct(id));
  };
  return (
    <Container>
      <Title>Products List</Title>
      {error ? <Error>Error Displaying Products</Error> : ""}
      <Row>
        <Button as="a" href="/admin/product/create">
          Create Product
        </Button>
      </Row>
      <CDataTable
        items={products}
        fields={fields}
        columnFilter
        tableFilter
        footer
        itemsPerPageSelect
        itemsPerPage={8}
        hover
        sorter
        pagination
        scopedSlots={{
          delete: (item, index) => {
            return (
              <td className="py-2">
                <CButton
                  color="danger"
                  size="sm"
                  onClick={() => {
                    handleDelete(item._id);
                  }}
                >
                  <DeleteIcon />
                </CButton>
              </td>
            );
          },
          prodImage: (item, index) => {
            return (
              <td className="py-2">
                <CImg
                  src={item.prodImage}
                  style={{ height: "60px", width: "80px" }}
                />
              </td>
            );
          },
          rating: (item, index) => {
            return (
              <td className="py-2">
                <Rating value={item.rating} />
              </td>
            );
          },
          edit: (item, index) => {
            return (
              <td className="py-2">
                <CButton
                  color="info"
                  variant="outline"
                  shape="square"
                  size="sm"
                  onClick={() => {
                    window.location.href = `/admin/product/${item._id}/edit`;
                  }}
                >
                  Edit
                </CButton>
              </td>
            );
          },
          show_details: (item, index) => {
            return (
              <td className="py-2">
                <CButton
                  color="primary"
                  variant="outline"
                  shape="square"
                  size="sm"
                  onClick={() => {
                    toggleDetails(index);
                  }}
                >
                  {details.includes(index) ? "Hide" : "Show"}
                </CButton>
              </td>
            );
          },
          details: (item, index) => {
            return (
              <CCollapse show={details.includes(index)}>
                <CCardBody>
                  <Details>
                    <Row>
                      <Col md={8}>
                        <h4>{item.name}</h4>
                        <Rating value={item.rating} />

                        <p className="text-muted">
                          Added on: {item.createdAt.substring(0, 10)}
                        </p>
                        <p> {item.description}</p>
                        <h6>Reviews:</h6>
                        <p>
                          {item.reviews.map((review) => (
                            <>
                              <h6>{review.name}</h6>
                              <p>{review.comment}</p>
                            </>
                          ))}
                        </p>
                      </Col>
                      <Col md={4}>
                        <CImg
                          src={item.prodImage}
                          style={{ height: "250px", width: "250px" }}
                        />
                      </Col>
                    </Row>
                  </Details>
                </CCardBody>
              </CCollapse>
            );
          },
        }}
      />
    </Container>
  );
};

export default ProductScreen;
