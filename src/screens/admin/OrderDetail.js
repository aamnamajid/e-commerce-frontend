import React, { useEffect, useState } from "react";
import { useParams } from "react-router";
import { useDispatch, useSelector } from "react-redux";
import { getOrderDetails, markOrderDeliver } from "../../actions/orderAction";
import { ADMIN_DELIVERED_ORDERS_RESET } from "../../constants/orderConstants";
import {
  Row,
  Col,
  ListGroup,
  Image,
  Card,
  Alert,
  Button,
} from "react-bootstrap";
import { Link } from "react-router-dom";
import {
  Container,
  Error,
  Title,
} from "./components/styledcomponents/global.styled";

const OrderDetail = () => {
  const { orderId } = useParams();
  const dispatch = useDispatch();

  const orderDetails = useSelector((state) => state.orderDetails);
  const { order, error } = orderDetails;

  const orderDeliver = useSelector((state) => state.orderDeliever);
  const { success } = orderDeliver;

  const [isLoading, setLoading] = useState(true);

  useEffect(() => {
    if (!order || order._id !== orderId || success) {
      dispatch({ type: ADMIN_DELIVERED_ORDERS_RESET });
      dispatch(getOrderDetails(orderId));
      setLoading(false);
    }
  }, [order, orderId, success, dispatch]);

  const deliverHandler = () => {
    dispatch(markOrderDeliver(order));
  };

  if (isLoading) {
    return (
      <div>
        <p>Loading...</p>
      </div>
    );
  } else {
    return (
      <Container>
        <Title>Order ID: {order._id}</Title>
        {error ? <Error>Error showing order Details</Error> : ""}
        <ListGroup variant="flush" className="mt-5">
          <h5>Shipping Address</h5>

          {order.user && (
            <ListGroup.Item>
              <Row>
                <h6>Customer name: {order.user.name}</h6>
              </Row>
              <Row>
                <h6>Customer Email: {order.user.email}</h6>
              </Row>
              <Row>
                <h6>
                  Customer Address:{order.shippingAddress.address},{" "}
                  {order.shippingAddress.city},{" "}
                  {order.shippingAddress.postalCode}
                </h6>
              </Row>
              {order.isDelievered ? (
                <Row>
                  <Alert variant="success">Delivered</Alert>
                </Row>
              ) : (
                <>
                  <>
                    <Alert variant="danger">Not Delivered</Alert>
                  </>
                  <Row>
                    <Button onClick={deliverHandler}>Mark as Delivered</Button>
                  </Row>
                </>
              )}
            </ListGroup.Item>
          )}
        </ListGroup>

        <ListGroup variant="flush" className="mt-5">
          <h5>Payment</h5>
          <>
            {order.isPaid ? (
              <Alert variant="success">Paid</Alert>
            ) : (
              <Alert variant="danger">Not Paid</Alert>
            )}
          </>
        </ListGroup>

        <ListGroup.Item className="mt-5">
          <h5>Order Items</h5>

          <ListGroup variant="flush">
            {order.orderItems &&
              order.orderItems.map((item, index) => (
                <>
                  <ListGroup.Item key={index}>
                    <Row>
                      <Col size={1}>
                        <Image
                          style={{ width: "50px", height: "40px" }}
                          src={item.image}
                          alt={item.name}
                          fluid
                          rounded
                        />
                      </Col>
                      <Col>
                        <Link to={`/product/${item.productID}`}>
                          {item.name}
                        </Link>
                      </Col>
                      <Col size={4}>
                        {item.qty} x ${item.price} = ${item.qty * item.price}
                      </Col>
                    </Row>
                  </ListGroup.Item>
                </>
              ))}
          </ListGroup>
        </ListGroup.Item>
        <ListGroup variant="flush" className="mt-5">
          {order && (
            <Card>
              <ListGroup.Item>
                <h5>Order Summary</h5>
              </ListGroup.Item>
              <ListGroup.Item>
                <Row>
                  <Col>Items</Col>
                  <Col>Rs{order.itemsPrice}</Col>
                </Row>
              </ListGroup.Item>
              <ListGroup.Item>
                <Row>
                  <Col>Shipping</Col>
                  <Col>Rs{order.shippingPrice}</Col>
                </Row>
              </ListGroup.Item>
              <ListGroup.Item>
                <Row>
                  <Col>Tax</Col>
                  <Col>Rs{order.taxPrice}</Col>
                </Row>
              </ListGroup.Item>
              <ListGroup.Item>
                <Row>
                  <Col>Total</Col>
                  <Col>Rs{order.totalPrice}</Col>
                </Row>
              </ListGroup.Item>
            </Card>
          )}
        </ListGroup>
      </Container>
    );
  }
};

export default OrderDetail;
