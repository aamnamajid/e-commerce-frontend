import { Link, NavLink } from "react-router-dom";
import HomeIcon from "@mui/icons-material/Home";
import PersonIcon from "@mui/icons-material/Person";
import LogoutIcon from "@mui/icons-material/Logout";
import ListAltIcon from "@mui/icons-material/ListAlt";
import ShoppingBagIcon from "@mui/icons-material/ShoppingBag";
import { Button } from "react-bootstrap";
import {
  SideBarTitle,
  SideBar,
  SidebarWrapper,
  SidebarMenu,
  SidebarList,
  SidebarListItemLogout,
} from "./styledcomponents/sidebar.styled";
import styled from "styled-components";
import { useDispatch } from "react-redux";
import { logout } from "../../../actions/userActions";

export const LinkElem = styled(NavLink)`
  color: white;
`;
export const Btn = styled(Button)`
  background-color: #b6e1e0;
  border-color: #b6e1e0
  color: black
  &:hover {
    background-color: #b6e1e0;
    border-color: #b6e1e0
    color: black
  }
`;

export const Links = styled(Link)`
  color: white;
  text-decoration: none;
`;

export const SidebarListItem = styled.li`
  padding: 5px;
  color: white
  cursor: pointer;
  display: flex;
  align-items: center;
  border-radius: 10px;
  ${Links}:hover & {
    color: black;
  }
  ${Links}.active & {
    background-color: white;
    color: red
  }
`;

export default function Sidebar() {
  const dispatch = useDispatch();

  const logouthandler = () => {
    dispatch(logout());
    window.location.href = "/";
  };
  return (
    <SideBar>
      <SidebarWrapper>
        <SidebarMenu>
          <SideBarTitle>Dashboard</SideBarTitle>
          <SidebarList>
            <Links to="/admin" style={{ textDecoration: "none" }}>
              <SidebarListItem>
                <HomeIcon />
                Home
              </SidebarListItem>
            </Links>
          </SidebarList>
          <SidebarList>
            <Links to="/admin/users" style={{ textDecoration: "none" }}>
              <SidebarListItem>
                <PersonIcon />
                Users
              </SidebarListItem>
            </Links>
          </SidebarList>
          <SidebarList>
            <Links to="/admin/product" style={{ textDecoration: "none" }}>
              <SidebarListItem>
                <ShoppingBagIcon />
                Products
              </SidebarListItem>
            </Links>
          </SidebarList>
          <SidebarList>
            <Links to="/admin/orders" style={{ textDecoration: "none" }}>
              <SidebarListItem>
                <ListAltIcon />
                Orders
              </SidebarListItem>
            </Links>
          </SidebarList>
        </SidebarMenu>

        <SidebarMenu>
          <SidebarList>
            <SidebarListItemLogout
              onClick={logouthandler}
              style={{ color: "white" }}
            >
              <LogoutIcon /> Logout
            </SidebarListItemLogout>
          </SidebarList>
        </SidebarMenu>
      </SidebarWrapper>
    </SideBar>
  );
}
