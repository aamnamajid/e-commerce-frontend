import React, { useEffect, useState } from "react";
import styled from "styled-components";
import { useDispatch, useSelector } from "react-redux";
import { deleteUser, getUsersList } from "../../../actions/userActions";
import DeleteIcon from "@mui/icons-material/Delete";
import { CDataTable, CCardBody, CCollapse, CButton } from "@coreui/react";
import { Row } from "react-bootstrap";
import { Container, Title, Button } from "./styledcomponents/global.styled";

const Details = styled.div`
  text-align: left !important;
  font-size: 14px;
`;

const UserList = () => {
  const dispatch = useDispatch();

  const userList = useSelector((state) => state.userList);
  const { users } = userList;

  const deletedUser = useSelector((state) => state.delUser);
  const { success } = deletedUser;

  useEffect(() => {
    dispatch(getUsersList());
  }, [success, dispatch]);
  const [details, setDetails] = useState([]);

  const toggleDetails = (index) => {
    const position = details.indexOf(index);
    let newDetails = details.slice();
    if (position !== -1) {
      newDetails.splice(position, 1);
    } else {
      newDetails = [...details, index];
    }
    setDetails(newDetails);
  };

  const fields = [
    { key: "name", _style: { width: "20%" } },
    { key: "email", _style: { width: "20%" } },
    {
      key: "delete",
      label: "",
      _style: { width: "1%" },
      sorter: false,
      filter: false,
    },
    {
      key: "show_details",
      label: "",
      _style: { width: "1%" },
      sorter: false,
      filter: false,
    },
    {
      key: "edit",
      label: "",
      _style: { width: "1%" },
      sorter: false,
      filter: false,
    },
  ];

  const handleDelete = (id) => {
    dispatch(deleteUser(id));
  };
  return (
    <Container>
      <Title>Users List</Title>
      <Row>
        <Button as="a" href="/admin/users/create">
          Create User
        </Button>
      </Row>
      <CDataTable
        items={users}
        fields={fields}
        columnFilter
        tableFilter
        footer
        itemsPerPageSelect
        itemsPerPage={5}
        hover
        sorter
        pagination
        scopedSlots={{
          delete: (item, index) => {
            return (
              <td className="py-2">
                <CButton
                  color="danger"
                  size="sm"
                  onClick={() => {
                    handleDelete(item._id);
                  }}
                >
                  <DeleteIcon />
                </CButton>
              </td>
            );
          },
          edit: (item, index) => {
            return (
              <td className="py-2">
                <CButton
                  color="info"
                  variant="outline"
                  shape="square"
                  size="sm"
                  onClick={() => {
                    window.location.href = `/admin/users/${item._id}/edit`;
                  }}
                >
                  Edit
                </CButton>
              </td>
            );
          },
          show_details: (item, index) => {
            return (
              <td className="py-2">
                <CButton
                  color="primary"
                  variant="outline"
                  shape="square"
                  size="sm"
                  onClick={() => {
                    toggleDetails(index);
                  }}
                >
                  {details.includes(index) ? "Hide" : "Show"}
                </CButton>
              </td>
            );
          },
          details: (item, index) => {
            return (
              <CCollapse show={details.includes(index)}>
                <CCardBody>
                  <Details>
                    <h4>{item.name}</h4>
                    <p>User since: {item.createdAt.substring(0, 10)}</p>
                    <p>Profile Updated on: {item.updatedAt.substring(0, 10)}</p>
                  </Details>
                </CCardBody>
              </CCollapse>
            );
          },
        }}
      />
    </Container>
  );
};

export default UserList;
