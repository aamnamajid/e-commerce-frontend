import styled from "styled-components";

export const FormContainer = styled.div`
  padding-right: 360px;
  margin-top: 50px;
`;

export const Button = styled.button`
  display: inline-block;
  color: white;
  background-color: teal;
  font-size: 1em;
  margin: 1em;
  padding: 0.25em 1em;
  border: 2px solid teal;
  border-radius: 3px;
  &:hover {
    cursor: pointer;
    color: white;
    text-decoration: none;
  }
`;
