import styled from "styled-components";

export const Grid = styled.div`
  padding-left: 80px;
  padding-right: 80px;
`;
export const Btn = styled.button`
  margin-right: 350px;
  color: white;
  background-color: #d10024;
  font-size: 1em;
  padding: 0.25em 1em;
  border: 2px solid #d10024;
  border-radius: 3px;
  &:hover {
    cursor: pointer;
    color: white;
    text-decoration: none;
  }
`;

export const Row = styled.div`
  display: flex;
  padding-right: 300px;
  text-align: left !important;
  @media (max-width: 550px) {
    flex-direction: column;
  }
`;

export const Col = styled.div`
  flex: ${(props) => props.size};
  margin-bottom: 20px;
`;
