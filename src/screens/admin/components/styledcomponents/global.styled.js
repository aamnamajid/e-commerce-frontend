import styled from "styled-components";

export const Container = styled.div`
  margin: 20px;
  padding-left: 50px;
  padding-right: 50px;
`;
export const Title = styled.h4`
  font-weight: bold;
  text-align: left !important;
  margin-top: 30px;
  margin-bottom: 20px;
`;
export const Message = styled.p`
  font-weight: bold;
  background-color: green;
  color: white;
`;

export const Button = styled.button`
  display: inline-block;
  color: white;
  background-color: #d10024;
  font-size: 1em;
  margin: 1em;
  padding: 0.25em 1em;
  border: 2px solid #d10024;
  border-radius: 3px;
  &:hover {
    cursor: pointer;
    color: white;
    text-decoration: none;
  }
`;

export const Error = styled.h5`
  color: red;
`;
export const Details = styled.div`
  text-align: left !important;
  font-size: 14px;
`;
