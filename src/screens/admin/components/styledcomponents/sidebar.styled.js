import styled from "styled-components";
export const SideBar = styled.div`
  flex: 1;
  height: 170vh;
  background-color: #d62546;
  position: sticky;
`;
export const SidebarWrapper = styled.div`
  padding: 20px;
  color: #555;
`;
export const SidebarMenu = styled.div`
  margin-bottom: 10px;
`;
export const SideBarTitle = styled.h3`
  font-size: 13px;
  color: white;
`;
export const SidebarList = styled.ul`
  list-style: none;
  padding: 5px;
`;
export const SidebarListItem = styled.li`
  padding: 5px;
  color: white
  cursor: pointer;
  display: flex;
  align-items: center;
  border-radius: 10px;
  &:hover {
    text-decoration: none
  }
  &.active {
    background-color: blue;
  }
`;
export const SidebarListItemLogout = styled.li`
  padding: 5px;
  color: white
  cursor: pointer;
  display: flex;
  align-items: center;
  border-radius: 10px;
  &:hover {
    cursor: pointer
  }
`;
