import React, { useEffect } from "react";
import styled from "styled-components";
import { useDispatch, useSelector } from "react-redux";
import { getAllOrdersAdmin } from "../../../actions/orderAction";

const Container = styled.div`
  margin-top: 15px;
  width: 100%;
  display: flex;
  justify-content: space-between;
`;
const FeaturedItem = styled.div`
  flex: 1;
  margin: 0px 60px;
  padding: 30px;
  border-radius: 10px;
  cursor: pointer;
  -webkit-box-shadow: 0px 0px 15px -10px rgba(0, 0, 0, 0.75);
  box-shadow: 0px 0px 15px -10px rgba(0, 0, 0, 0.75);
`;

const Title = styled.p`
  font-size: 20px;
`;

const FeaturedContainer = styled.div`
  margin: 10px 0px;
  display: flex;
  align-items: center;
  justify-content: center;
`;

const FeaturedMoney = styled.p`
  font-size: 30px;
  font-weight: 600;
`;

const Infocard = () => {
  const dispatch = useDispatch();
  const ordersList = useSelector((state) => state.allOrders);
  const {
    orders: { orders },
  } = ordersList;

  useEffect(() => {
    dispatch(getAllOrdersAdmin());
  }, [dispatch]);

  if (orders) {
    var ordersDel = orders.filter((o) => o.isDelievered === true).length;
    var ordersPending = orders.filter((o) => o.isDelievered === false).length;
    var totalSales = orders.reduce((acc, item) => acc + item.totalPrice, 0);
  }
  return (
    <Container>
      <FeaturedItem style={{ backgroundColor: "#20a8d8" }}>
        <Title>Sales</Title>
        <FeaturedContainer>
          <FeaturedMoney>${totalSales}</FeaturedMoney>
        </FeaturedContainer>
      </FeaturedItem>
      <FeaturedItem style={{ backgroundColor: "#ffc107" }}>
        <Title>Orders Delivered </Title>
        <FeaturedContainer>
          <FeaturedMoney>{ordersDel}</FeaturedMoney>
        </FeaturedContainer>
      </FeaturedItem>
      <FeaturedItem
        style={{
          backgroundColor: "#f86c6b",
        }}
      >
        <Title>Pending Orders</Title>
        <FeaturedContainer>
          <FeaturedMoney>{ordersPending}</FeaturedMoney>
        </FeaturedContainer>
      </FeaturedItem>
    </Container>
  );
};

export default Infocard;
