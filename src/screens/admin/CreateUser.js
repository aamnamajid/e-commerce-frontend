import React, { useEffect, useState } from "react";
import { useFormik } from "formik";
import { useSelector, useDispatch } from "react-redux";
import { createUserByAdmin } from "../../actions/userActions";
import { FormContainer } from "./components/styledcomponents/createuser.styled";
import {
  Container,
  Title,
  Message,
  Button,
} from "./components/styledcomponents/global.styled";

const validate = (values) => {
  const errors = {};

  if (!values.password) {
    errors.password = "* Password is required";
  } else if (values.password.length < 6) {
    errors.password = "Must be 6 characters or greater";
  }

  if (!values.email) {
    errors.email = "* Email is required";
  } else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)) {
    errors.email = "Invalid email address";
  }

  return errors;
};

const CreateUser = () => {
  const dispatch = useDispatch();
  const userCreated = useSelector((state) => state.createdUser);
  const { success, error } = userCreated;
  const [message, setMessage] = useState("");

  useEffect(() => {
    if (success) {
      setMessage("User Created");
      window.location.href = "/admin/users";
    }
  }, [success]);

  const formik = useFormik({
    initialValues: {
      name: "",
      password: "",
      email: "",
    },
    validate,
    onSubmit: async (values) => {
      const { name, email, password } = values;
      dispatch(createUserByAdmin(name, email, password));
    },
  });

  return (
    <Container>
      <Title>Create User</Title>
      <FormContainer>
        <form onSubmit={formik.handleSubmit}>
          <Message>{message ? message : ""}</Message>
          {error && <p style={{ color: "red" }}>{error}</p>}
          <div className="form-group text-left">
            <label htmlFor="name">Name</label>
            <input
              id="name"
              name="name"
              type="name"
              className="form-control"
              onChange={formik.handleChange}
              value={formik.values.name}
            />
            {formik.errors.name ? (
              <div style={{ color: "red" }}>{formik.errors.name}</div>
            ) : null}
          </div>
          <div className="form-group text-left">
            <label htmlFor="email">Email Address</label>
            <input
              id="email"
              name="email"
              type="email"
              className="form-control"
              onChange={formik.handleChange}
              value={formik.values.email}
            />
            {formik.errors.email ? (
              <div style={{ color: "red" }}>{formik.errors.email}</div>
            ) : null}
          </div>

          <div className="form-group  text-left">
            <label htmlFor="password">Password</label>
            <input
              id="password"
              className="form-control"
              name="password"
              type="password"
              onChange={formik.handleChange}
              value={formik.values.password}
            />
            {formik.errors.password ? (
              <div style={{ color: "red" }}>{formik.errors.password}</div>
            ) : null}
          </div>

          <div>
            <Button type="submit">Register</Button>
          </div>
        </form>
      </FormContainer>
    </Container>
  );
};

export default CreateUser;
