import React from "react";
import { useFormik } from "formik";
import {
  Button,
  Error,
  FormGroup,
  Row,
  Col,
  Form,
  Input,
  FormLabel,
  Container,
  Title,
} from "../components/styledComponents/global.styled";
import { useDispatch } from "react-redux";
import { shippingAddress } from "../actions/cartActions";
import { useNavigate } from "react-router";
import styled from "styled-components";
export const Cont = styled(Container)`
  padding: 0 200px;
  margin-top: 100px;
  @media (max-width: 768px) {
    padding: 0;
  }
`;

const validate = (values) => {
  const errors = {};

  if (!values.address) {
    errors.address = "* address is required";
  }

  if (!values.city) {
    errors.city = "* city is required";
  }

  if (!values.postalCode) {
    errors.postalCode = "* postalCode is required";
  }

  return errors;
};

const Shipping = () => {
  const navigate = useNavigate();

  const dispatch = useDispatch();
  const formik = useFormik({
    initialValues: {
      city: "",
      postalCode: "",
      address: "",
    },
    validate,
    onSubmit: async (values) => {
      const { city, postalCode, address } = values;
      dispatch(shippingAddress({ city, postalCode, address }));
      window.location.href = "/order/place-order";
    },
  });

  return (
    <Cont>
      <Title>SHIPPING DETAILS</Title>

      <Row>
        <Col size={10}>
          <Form onSubmit={formik.handleSubmit}>
            <FormGroup>
              <FormLabel htmlFor="address">Address</FormLabel>
              <Input
                id="address"
                address="address"
                type="text"
                onChange={formik.handleChange}
                value={formik.values.address}
              />
              {formik.errors.address ? (
                <Error>{formik.errors.address}</Error>
              ) : null}
            </FormGroup>
            <FormGroup>
              <FormLabel htmlFor="city">City</FormLabel>
              <Input
                id="city"
                name="city"
                type="city"
                onChange={formik.handleChange}
                value={formik.values.city}
              />
              {formik.errors.city ? <Error>{formik.errors.city}</Error> : null}
            </FormGroup>

            <FormGroup>
              <FormLabel htmlFor="postalCode">Postal Code</FormLabel>
              <Input
                id="postalCode"
                name="postalCode"
                type="postalCode"
                onChange={formik.handleChange}
                value={formik.values.postalCode}
              />
              {formik.errors.postalCode ? (
                <Error>{formik.errors.postalCode}</Error>
              ) : null}
            </FormGroup>

            <div>
              <Button type="submit">Procceed</Button>
            </div>
          </Form>
        </Col>
      </Row>
    </Cont>
  );
};

export default Shipping;
