import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import ClearIcon from "@mui/icons-material/Clear";
import CheckIcon from "@mui/icons-material/Check";
import { CDataTable, CButton, CFormText } from "@coreui/react";
import { myOrdersList } from "../actions/orderAction";
import { Container, Title } from "../components/styledComponents/global.styled";

const MyOrder = () => {
  const dispatch = useDispatch();
  const userDetails = useSelector((state) => state.userDetails);
  const { user } = userDetails;

  const userLogin = useSelector((state) => state.userLogin);
  const { userInfo } = userLogin;

  const myOrders = useSelector((state) => state.myOrderList);
  const { orders } = myOrders;

  useEffect(() => {
    if (!userInfo) {
      window.location.href = "/login";
    } else {
      if (!user.name) {
        dispatch(myOrdersList());
      }
    }
  }, [dispatch, userInfo, user]);

  const fields = [
    { key: "_id", _style: { width: "5%" } },
    { key: "totalPrice", _style: { width: "20%" } },
    { key: "isDelievered", label: "Delievered", _style: { width: "20%" } },
    { key: "createdAt", _style: { width: "20%" } },

    {
      key: "show_details",
      label: "",
      _style: { width: "1%" },
      sorter: false,
      filter: false,
    },
  ];

  return (
    <Container>
      <Title>My Orders List</Title>

      <CDataTable
        items={orders}
        fields={fields}
        columnFilter
        tableFilter
        footer
        itemsPerPageSelect
        itemsPerPage={8}
        hover
        sorter
        pagination
        scopedSlots={{
          createdAt: (item, index) => {
            return (
              <td className="py-2">
                <CFormText>{item.createdAt.substring(0, 10)}</CFormText>
              </td>
            );
          },

          isDelievered: (item, index) => {
            return (
              <td className="py-2">
                {item.isDelievered ? <CheckIcon /> : <ClearIcon />}
              </td>
            );
          },
          show_details: (item, index) => {
            return (
              <td className="py-2">
                <CButton
                  color="danger"
                  shape="square"
                  size="sm"
                  onClick={() => {
                    window.location.href = `/order/${item._id}`;
                  }}
                >
                  Details
                </CButton>
              </td>
            );
          },
        }}
      />
    </Container>
  );
};

export default MyOrder;
