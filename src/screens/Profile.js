import React, { useEffect, useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import { Button, Error } from "../components/styledComponents/global.styled";
import { getUserDetail, updateUserprofile } from "../actions/userActions";
import styled from "styled-components";
import { Form } from "react-bootstrap";
import { myOrdersList } from "../actions/orderAction";

export const Grid = styled.div`
  padding-left: 80px;
  padding-right: 80px;
  margin-top: 80px;
`;
export const Row = styled.div`
  display: flex;
  text-align: left !important;
  @media (max-width: 550px) {
    flex-direction: column;
  }
`;

export const Col = styled.div`
  flex: ${(props) => props.size};
  margin: 15px;
`;
const Title = styled.h4`
  font-weight: bold;
  text-align: left !important;
  margin-top: 30px;
  margin-bottom: 20px;
`;

const Message = styled.p`
  font-weight: bold;
  background-color: green;
  color: white;
`;

const Profile = () => {
  const dispatch = useDispatch();
  const userDetails = useSelector((state) => state.userDetails);
  const { error, user } = userDetails;

  const userLogin = useSelector((state) => state.userLogin);
  const { userInfo } = userLogin;

  const updateProfile = useSelector((state) => state.updateProfile);
  const { userUpdated } = updateProfile;

  const [name, setName] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [confirmPassword, setConfirmPassword] = useState("");
  const [message, setMessage] = useState(null);

  useEffect(() => {
    if (userUpdated) {
      setMessage("Profile Updated Successfully");
    }
  }, [userUpdated]);

  useEffect(() => {
    if (!userInfo) {
      window.location.href = "/login";
    } else {
      if (!user.name) {
        dispatch(getUserDetail("profile"));
        dispatch(myOrdersList());
      } else {
        setName(user.name);
        setEmail(user.email);
      }
    }
  }, [dispatch, userInfo, user]);

  const submitHandler = (e) => {
    e.preventDefault();
    if (password !== confirmPassword) {
      setMessage("Passwords do not match");
    } else {
      dispatch(updateUserprofile({ id: user._id, name, email, password }));
    }
  };

  return (
    <Grid>
      <Title>User Profile</Title>
      <Error>{error ? error : ""}</Error>
      <Message>{message ? message : ""}</Message>
      <Form onSubmit={submitHandler}>
        <Row>
          <Col size={6}>
            <Form.Group controlId="name">
              <Form.Label>Name</Form.Label>
              <Form.Control
                type="name"
                placeholder="Enter name"
                value={name}
                onChange={(e) => setName(e.target.value)}
              ></Form.Control>
            </Form.Group>
          </Col>
          <Col size={6}>
            <Form.Group controlId="email">
              <Form.Label>Email Address</Form.Label>
              <Form.Control
                type="email"
                placeholder="Enter email"
                value={email}
                onChange={(e) => setEmail(e.target.value)}
              ></Form.Control>
            </Form.Group>
          </Col>
        </Row>
        <Row>
          <Col size={6}>
            <Form.Group controlId="password">
              <Form.Label>Password</Form.Label>
              <Form.Control
                type="password"
                placeholder="Enter password"
                value={password}
                onChange={(e) => setPassword(e.target.value)}
              ></Form.Control>
            </Form.Group>
          </Col>
          <Col size={6}>
            <Form.Group controlId="confirmPassword">
              <Form.Label>Confirm Password</Form.Label>
              <Form.Control
                type="password"
                placeholder="Confirm password"
                value={confirmPassword}
                onChange={(e) => setConfirmPassword(e.target.value)}
              ></Form.Control>
            </Form.Group>
          </Col>
        </Row>

        <Button type="submit">Update</Button>
      </Form>
    </Grid>
  );
};

export default Profile;
