import { createStore, applyMiddleware, combineReducers } from "redux";
import thunk from "redux-thunk";
import { composeWithDevTools } from "redux-devtools-extension";
import {
  addReviewReducer,
  adminCreateProduct,
  adminGetProductReducer,
  adminUpdateProduct,
  delProduct,
  productReducer,
} from "./reducers/productReducer";
import {
  adminCreateUser,
  adminDelUser,
  adminGetUserList,
  adminUpdateUser,
  userDetailReducer,
  userLoginReducer,
  userRegisterReducer,
  userUpdateProfileReducer,
} from "./reducers/userReducer";
import {
  productDetailReducer,
  TopProductsReducer,
} from "./reducers/productReducer";
import { cartReducer } from "./reducers/cartReducer";
import {
  orderReducer,
  orderDetailsReducer,
  orderListReducer,
  adminOrderListReducer,
  adminOrderDeliverReducer,
  orderPayReducer,
  adminDelOrder,
} from "./reducers/orderReducer";

const reducer = combineReducers({
  allProducts: productReducer,
  userLogin: userLoginReducer,
  userRegister: userRegisterReducer,
  userDetails: userDetailReducer,
  updateProfile: userUpdateProfileReducer,
  createdUser: adminCreateUser,
  delUser: adminDelUser,
  userList: adminGetUserList,
  updateUser: adminUpdateUser,
  deleteProduct: delProduct,
  productDetails: productDetailReducer,
  createProduct: adminCreateProduct,
  updateProduct: adminUpdateProduct,
  getProducts: adminGetProductReducer,
  productReview: addReviewReducer,
  cart: cartReducer,
  order: orderReducer,
  orderDeliever: adminOrderDeliverReducer,
  myOrderList: orderListReducer,
  orderDetails: orderDetailsReducer,
  payOrder: orderPayReducer,
  allOrders: adminOrderListReducer,
  delOrder: adminDelOrder,
  topProducts: TopProductsReducer,
});

const userInfo = localStorage.getItem("userInfo")
  ? JSON.parse(localStorage.getItem("userInfo"))
  : null;
const orderItems = localStorage.getItem("orderItems")
  ? JSON.parse(localStorage.getItem("orderItems"))
  : [];
const shippingAddress = localStorage.getItem("shippingAddress")
  ? JSON.parse(localStorage.getItem("shippingAddress"))
  : [];

const initialState = {
  userLogin: { userInfo },
  cart: { orderItems, shippingAddress },
};

const middleware = [thunk];
const store = createStore(
  reducer,
  initialState,
  composeWithDevTools(applyMiddleware(...middleware))
);

export default store;
